# PAH-2021

#### General:

Flutter: >2.5.3

Dart:    >=2.14.0 <3.0.0

## Getting Started

#### Install dependencies
1. (Optionally) use install FVM with `dart pub global activate fvm`. If you're using FVM, prefix any flutter command with ``fvm``.
1. Navigate to `ph-risk-score` and type `flutter pub get`

#### Run code generation
1. Navigate to `ph-risk-score` and run build_runner

### Build runner
#### watch task:
1. run ``flutter pub run build_runner watch --delete-conflicting-outputs``

#### build task:
1. run ``flutter pub run build_runner build --delete-conflicting-outputs``

### Building:
#### Web:
run `fvm flutter pub run bin/build.dart` from the project root. The script will create two tar files called `web_standard.tar.gz` and `web_infant.tar.gz` containing the different configurations.

#### Android APK:
run ``flutter build apk``
