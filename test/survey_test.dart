import 'package:flutter_test/flutter_test.dart';
import 'package:phindex/models/enums.dart';
import 'package:phindex/pages/survey_page/survey_page_store.dart';
import 'package:phindex/util/formula.dart';
import 'package:phindex/util/util.dart';

void main() {
  // INFO: these are only tests for getter calculating any sort of level
  group("Getter", () {
    test("Age", () {
      final store = SurveyStore(false);
      store.dateOfBirth = DateTime(2009, 01, 24);
      store.dateOfClinicalPresentation = DateTime(2014, 10, 13);
      expect(store.age, 5.5);
    });
    test("BodySurfaceArea", () {
      final store = SurveyStore(false);
      store.height = 190;
      store.weight = 80;
      expect(store.bodySurfaceArea, 2.05);
    });
    test("BodyMassIndex", () {
      final store = SurveyStore(false);
      store.height = 190;
      store.weight = 80;
      expect(store.bodyMassIndex, 22.2);
    });
    test("BMIPercentile", () {
      final store = SurveyStore(false);
      store.sex = Sex.male;
      store.dateOfBirth = DateTime(2009, 01, 24);
      store.dateOfClinicalPresentation = DateTime(2014, 10, 13);
      store.weight = 20.0;
      store.height = 111;
      expect(store.bodyMassIndex, 16.2);
      expect(store.bodySurfaceArea, 0.79);
      expect(store.bmiPercentile?.p3, 13.2);
      expect(store.bmiPercentile?.p10, 13.8);
    });
    test("HeightPercentile", () {
      final store = SurveyStore(false);
      store.sex = Sex.male;
      store.dateOfBirth = DateTime(2009, 01, 24);
      store.dateOfClinicalPresentation = DateTime(2014, 10, 13);
      store.height = 111;
      expect(store.height, 111);
      expect(store.heightPercentile?.p3, 105.77);
      expect(store.heightPercentile?.p10, 108.74);
    });
    test("Growth", () {
      final store = SurveyStore(false);
      store.sex = Sex.male;
      store.dateOfBirth = DateTime(2009, 01, 24);
      store.dateOfClinicalPresentation = DateTime(2014, 10, 13);
      store.height = 111;
      store.weight = 20;
      expect(store.growth, Growth.normal);
    });
    test("SerumNTproBNPLevel", () {
      final store = SurveyStore(false);
      store.serumNTproBNP = 1300;
      expect(store.serumNTproBNPLevel, SerumNTproBNP.greatlyElevated);
      store.serumNTproBNP = 100;
      expect(store.serumNTproBNPLevel, SerumNTproBNP.minimallyElevated);
      store.serumNTproBNP = 300;
      expect(store.serumNTproBNPLevel, SerumNTproBNP.moderatedlyElevated);
    });
    test("RvlvEndsystolicRatioLevel", () {
      final store = SurveyStore(false);
      store.rvlvEndsystolicRatio = 0.8;
      expect(store.rvlvEndsystolicRatioLevel, RVLVEndsystolicRatio.below1);
      store.rvlvEndsystolicRatio = 3;
      expect(store.rvlvEndsystolicRatioLevel, RVLVEndsystolicRatio.severe);
      store.rvlvEndsystolicRatio = 1.3;
      expect(store.rvlvEndsystolicRatioLevel, RVLVEndsystolicRatio.between1And1Point5);
    });
    test("SdRatioLevel", () {
      final store = SurveyStore(false);
      store.sdRatio = null;
      expect(store.sdRatioLevel, null);
      store.sdRatio = 0.5;
      expect(store.sdRatioLevel, SDRatioLevel.below1);
      store.sdRatio = 1.2;
      expect(store.sdRatioLevel, SDRatioLevel.between1And1point4);
      store.sdRatio = 4.2;
      expect(store.sdRatioLevel, SDRatioLevel.above1Point4);
    });
    test("PaatLevel", () {
      final store = SurveyStore(false);
      store.paat = 50;
      expect(store.paatLevel, PAATLevel.below70);
      store.paat = 80;
      expect(store.paatLevel, PAATLevel.between70And100);
      store.paat = 300;
      expect(store.paatLevel, PAATLevel.over100);
    });
    test("LastCathStudyValid", () {
      final store = SurveyStore(false);
      store.dateOfLastCathStudy = DateTime.now().add(const Duration(days: 2));
      expect(store.lastCathStudyValid, false);
      store.dateOfLastCathStudy = DateTime.now().add(const Duration(days: -2));
      expect(store.lastCathStudyValid, true);
    });
    test("CaridacIndexLevel", () {
      final store = SurveyStore(false);
      store.cardiacIndex = 4;
      expect(store.cardiacIndexLevel, CardiacIndexLevel.over3);
      store.cardiacIndex = 2.7;
      expect(store.cardiacIndexLevel, CardiacIndexLevel.between3And2Point5);
      store.cardiacIndex = 1.8;
      expect(store.cardiacIndexLevel, CardiacIndexLevel.below2Point5);
    });
    test("MRap Level", () {
      final store = SurveyStore(false);
      store.mRAP = 5;
      expect(store.mRAPLevel, MRAPLevel.below10);
      store.mRAP = 10;
      expect(store.mRAPLevel, MRAPLevel.between10And15);
      store.mRAP = 15;
      expect(store.mRAPLevel, MRAPLevel.between10And15);
      store.mRAP = 15.1;
      expect(store.mRAPLevel, MRAPLevel.over15);
      store.mRAP = 20;
      expect(store.mRAPLevel, MRAPLevel.over15);
    });
    test("mPAP / SAPLevel & Level", () {
      final store = SurveyStore(false);
      store.mPAP = 1;
      store.mSAP = 4;
      expect(store.mPAPDividedByMSAP, 0.25);
      expect(store.mPAPmSAPLevel, MPAPmSAPLevel.below0Point5);
      store.mPAP = 1;
      store.mSAP = 2;
      expect(store.mPAPDividedByMSAP, 0.5);
      expect(store.mPAPmSAPLevel, MPAPmSAPLevel.between0Point5And0Point75);
      store.mPAP = 3;
      store.mSAP = 4;
      expect(store.mPAPDividedByMSAP, 0.75);
      expect(store.mPAPmSAPLevel, MPAPmSAPLevel.between0Point5And0Point75);
      store.mPAP = 4;
      store.mSAP = 4;
      expect(store.mPAPDividedByMSAP, 1);
      expect(store.mPAPmSAPLevel, MPAPmSAPLevel.above0Point75);
    });
    test("PvriAbove15WUxMSquared", () {
      final store = SurveyStore(false);
      store.pvri = null;
      expect(store.pvriAbove15WUxMSquared, false);
      store.pvri = 10;
      expect(store.pvriAbove15WUxMSquared, false);
      store.pvri = 22;
      expect(store.pvriAbove15WUxMSquared, true);
    });
  });

  // INFO: these are tests of util methods for calculating real values
  // Maybe we have to Adjust method. Internal CalZScore calculates with DateTime.now
  group("Utils", () {
    test("Calc Z Score", () {
      expect(calcZScore(DateTime(2014, 10, 13), DateTime(2009, 01, 24), 1.7), -1.27); // emily schlüter
      expect(calcZScore(DateTime(2019, 01, 24), DateTime(2003, 12, 06), 1.47), -4.35); // moritz werner
      expect(calcZScore(DateTime(2020, 07, 08), DateTime(2011, 07, 28), 2.23), 1.69); // Heijzala
      expect(calcZScore(DateTime(2015, 08, 12), DateTime(2004, 12, 16), 1.56), -3.69); // Shawn
      expect(calcZScore(DateTime(2015, 08, 12), DateTime(2004, 12, 16), 1.56), -3.69); // Beispiel
    });
    test("Calc Paat Z-Score", () {
      expect(calcPaatZScore(50, Sex.male, 11), -4.449716557119888);
      expect(calcPaatZScore(30, Sex.female, 16), -6.327397047397048);
      expect(calcPaatZScore(20, Sex.male, 18), -6.981014198695856);
      expect(calcPaatZScore(13, Sex.female, 8), -7.2972314290192015);
    });
    test("Calc Growth", () {
      final store = SurveyStore(false);
      store.sex = Sex.male;
      store.dateOfBirth = DateTime(2009, 01, 24);
      store.dateOfClinicalPresentation = DateTime(2014, 10, 13);
      store.height = 111;
      store.weight = 20;
      expect(calcGrowth(store.bodyMassIndex, store.bmiPercentile, store.height, store.heightPercentile), Growth.normal);
      store.sex = Sex.male;
      store.dateOfBirth = DateTime(2003, 12, 06);
      store.patientId = "207";
      store.weight = 44.0;
      store.height = 173;
      store.dateOfClinicalPresentation = DateTime(2019, 01, 24);
      expect(calcGrowth(store.bodyMassIndex, store.bmiPercentile, store.height, store.heightPercentile),
          Growth.failureToThrive);

      // fat (must be 17)
      store.sex = Sex.male;
      store.dateOfBirth = DateTime(2004, 12, 16);
      store.dateOfClinicalPresentation = DateTime(2018, 08, 12);
      store.height = 150;
      store.weight = 35;
      expect(calcGrowth(store.bodyMassIndex, store.bmiPercentile, store.height, store.heightPercentile),
          Growth.slightlyRestricted);
    });
  });
  group("Validation (Required)", () {
    test("Personal Data", () {
      final store = SurveyStore(false);
      store.dateOfBirth = DateTime(2019, 08, 12);
      store.weight = 80;
      store.height = 170;
      store.sex = Sex.male;
      expect(store.personalDataValid, true);
    });

    test("Clinical Presentation", () {
      final store = SurveyStore(false);
      store.dateOfClinicalPresentation = DateTime(2019, 08, 12);
      store.clinicalEvidenceOfRvFailure = false;
      store.progressionOfSymptoms = false;
      store.syncopeInTheLast6Months = true;
      store.whoFunctionalClass = WHOFunctionalClass.I;
      expect(store.clinicalPresentationValid, true);
    });

    test("Laboratory Results", () {
      final store = SurveyStore(false);
      store.dateOfBloodExamination = DateTime(2019, 08, 12);
      store.serumNTproBNP = 2.0;
      expect(store.laboratoryResultsValid, true);
    });

    test("Medical Imaging", () {
      final store = SurveyStore(false);
      store.dateOfEchoCardiography = DateTime(2019, 08, 12);
      store.rarvEnlargement = RARVEnlargement.minimal;
      store.rvSystolivDysfunction = RVSystolicDysfunction.noDysfunction;
      store.rvlvEndsystolicRatio = 12.0;
      store.tapse = 1.7;
      store.sdRatio = 2.5;
      store.paatAvailable = false;
      store.pericardialEffusion = PericardialEffusion.noPericardialEffusion;
      expect(store.medicalImagingValid, true);
    });
    test("Cardiac Catherization", () {
      final store = SurveyStore(false);
      store.cardiacCatheterizationAvailable = true;
      store.dateOfLastCathStudy = DateTime(2019, 08, 12);
      store.cardiacIndex = 1;
      store.mRAP = 10;
      store.mPAP = 3;
      store.mSAP = 3;
      store.acuteVasoreactivityTesting = AcuteVasoreactivityTesting.negative;
      store.pvri = 20.2;
      expect(store.cardiacCatheterizationValid, true);
    });
  });

  // INFO: These are full tests of 'real' patient data
  group("Full User Tests", () {

    test("Emily Schlüter", () {
      final store = SurveyStore(false);

      store.dateOfBirth = DateTime(2009, 01, 24);
      store.patientId = "201";
      store.weight = 20.0;
      store.height = 111;
      expect(store.bodyMassIndex, 16.2);
      expect(store.bodySurfaceArea, 0.79);

      store.sex = Sex.female;
      store.race = Race.white;
      store.country = "Germany";
      store.phClassification = PhClassification.IPAH;

      store.dateOfClinicalPresentation = DateTime(2014, 10, 13);
      store.clinicalEvidenceOfRvFailure = false;
      store.progressionOfSymptoms = false;
      store.syncopeInTheLast6Months = false;
      expect(store.growth, Growth.normal);
      store.whoFunctionalClass = WHOFunctionalClass.II;

      store.dateOfBloodExamination = DateTime(2014, 10, 09);
      store.serumNTproBNP = 50;
      store.dateOfEchoCardiography = DateTime(2014, 10, 09);
      store.rarvEnlargement = RARVEnlargement.minimal;
      store.rvSystolivDysfunction = RVSystolicDysfunction.noDysfunction;
      store.rvlvEndsystolicRatio = 1.6;
      store.tapse = 1.7;
      expect(store.zScore, -1.27);
      expect(store.tapseLevel, TAPSELevel.normal);
      store.sdRatio = 0.47;
      store.paat = 74;
      expect(store.paatLevel, PAATLevel.between70And100);
      store.pericardialEffusion = PericardialEffusion.noPericardialEffusion;

      store.cardiacCatheterizationAvailable = true;
      store.dateOfLastCathStudy = DateTime(2014, 10, 15);
      store.cardiacIndex = 3.33;
      store.mRAP = 3;
      store.mPAP = 80;
      store.mSAP = 77;
      expect(store.mPAPmSAPLevel, MPAPmSAPLevel.above0Point75);
      final mPapDiv = store.mPAPDividedByMSAP;
      if (mPapDiv == null) throw const FormatException("mPAPDividedByMSAP shall not be null");
      expect(mPapDiv.toPrecision(3), 1.039);
      store.acuteVasoreactivityTesting = AcuteVasoreactivityTesting.notAvailable;
      store.pvri = 21.5;

      // calculate test results
      store.evaluateBlocks();

      // =========== Test Results =============

      // non invasive
      expect(store.highRiskScore?.numerator, 1);
      expect(store.highRiskScore?.denominator, 15);
      expect(store.lowRiskScore?.numerator, 12);
      expect(store.lowRiskScore?.denominator, 14);

      // invasive results
      expect(store.lowRiskInvasiveScore?.numerator, 16);
      expect(store.lowRiskInvasiveScore?.denominator, 19);
      expect(store.highRiskInvasiveScore?.numerator, 3);
      expect(store.highRiskInvasiveScore?.denominator, 21);
    });
    test("Moritz Werner", () {
      final store = SurveyStore(false);

      store.dateOfBirth = DateTime(2003, 12, 06);
      store.patientId = "207";
      store.weight = 44.0;
      store.height = 173;
      expect(store.bodySurfaceArea, 1.45);
      expect(store.bodyMassIndex, 14.7);

      store.sex = Sex.male;
      store.race = Race.white;
      store.country = "Germany";
      store.phClassification = PhClassification.IPAH;

      store.dateOfClinicalPresentation = DateTime(2019, 01, 24);
      store.clinicalEvidenceOfRvFailure = false;
      store.progressionOfSymptoms = false;
      store.syncopeInTheLast6Months = false;
      expect(store.growth, Growth.failureToThrive);
      store.whoFunctionalClass = WHOFunctionalClass.II;

      store.dateOfBloodExamination = DateTime(2019, 01, 24);
      store.serumNTproBNP = 50;
      store.dateOfEchoCardiography = DateTime(2019, 01, 24);
      store.rarvEnlargement = RARVEnlargement.minimal;
      store.rvSystolivDysfunction = RVSystolicDysfunction.noDysfunction;
      store.rvlvEndsystolicRatio = 1.2;
      store.tapse = 1.47;
      expect(store.zScore, -4.35);
      expect(store.tapseLevel, TAPSELevel.belowMinus3);
      store.sdRatio = 1.6;
      store.paat = 80;
      store.pericardialEffusion = PericardialEffusion.noPericardialEffusion;

      store.cardiacCatheterizationAvailable = true;
      store.dateOfLastCathStudy = DateTime(2019, 01, 25);
      store.cardiacIndex = 2.57;
      store.mRAP = 1;
      store.mPAP = 61;
      store.mSAP = 68;
      final mPapDiv = store.mPAPDividedByMSAP;
      if (mPapDiv == null) throw const FormatException("mPAPDividedByMSAP shall not be null");
      expect(mPapDiv, 0.8970588235294118);
      store.acuteVasoreactivityTesting = AcuteVasoreactivityTesting.negative;
      store.pvri = 21.7;

      // =========== Test Results =============

      store.evaluateBlocks();
      // non invasive
      expect(store.highRiskScore?.numerator, 3);
      expect(store.highRiskScore?.denominator, 15);
      expect(store.lowRiskScore?.numerator, 9);
      expect(store.lowRiskScore?.denominator, 14);

      // invasive results
      expect(store.lowRiskInvasiveScore?.numerator, 11);
      expect(store.lowRiskInvasiveScore?.denominator, 20);
      expect(store.highRiskInvasiveScore?.numerator, 5);
      expect(store.highRiskInvasiveScore?.denominator, 21);
    });

    test("Heijzala Kurtishova", () {
      final store = SurveyStore(false);
      store.dateOfBirth = DateTime(2011, 07, 28);
      store.patientId = "284";
      store.weight = 33.0;
      store.height = 132;
      expect(store.bodySurfaceArea, 1.1);
      expect(store.bodyMassIndex, 18.9);

      store.sex = Sex.female;
      store.race = Race.white;
      store.country = "Germany";
      store.phClassification = PhClassification.HPAH;

      store.dateOfClinicalPresentation = DateTime(2020, 07, 08);
      store.clinicalEvidenceOfRvFailure = false;
      store.progressionOfSymptoms = false;
      store.syncopeInTheLast6Months = false;
      expect(store.growth, Growth.normal);
      store.whoFunctionalClass = WHOFunctionalClass.I;

      store.dateOfBloodExamination = DateTime(2020, 07, 08);
      store.serumNTproBNP = 45;
      store.dateOfEchoCardiography = DateTime(2019, 07, 08);
      store.rarvEnlargement = RARVEnlargement.minimal;
      store.rvSystolivDysfunction = RVSystolicDysfunction.noDysfunction;
      store.rvlvEndsystolicRatio = 0.82;
      store.tapse = 2.23;
      expect(store.zScore, 1.69);
      expect(store.tapseLevel, TAPSELevel.normal);
      store.sdRatio = 1.05;
      store.paat = 130;
      store.pericardialEffusion = PericardialEffusion.noPericardialEffusion;

      store.cardiacCatheterizationAvailable = true;
      store.dateOfLastCathStudy = DateTime(2020, 07, 09);
      store.cardiacIndex = 5.36;
      store.mRAP = 1;
      store.mPAP = 17;
      store.mSAP = 66;
      final mPapDiv = store.mPAPDividedByMSAP;
      if (mPapDiv == null) throw const FormatException("mPAPDividedByMSAP shall not be null");
      expect(mPapDiv, 0.25757575757575757);
      store.acuteVasoreactivityTesting = AcuteVasoreactivityTesting.negative;
      store.pvri = 2.18;

      // =========== Test Results =============
      store.evaluateBlocks();
      // non invasive
      expect(store.highRiskInvasiveScore?.numerator, 0);
      expect(store.highRiskInvasiveScore?.denominator, 21);
      expect(store.highRiskScore?.numerator, 0);
      expect(store.highRiskScore?.denominator, 15);

      expect(store.lowRiskInvasiveScore?.numerator, 18);
      expect(store.lowRiskInvasiveScore?.denominator, 20);
      expect(store.lowRiskScore?.numerator, 13);
      expect(store.lowRiskScore?.denominator, 14);


    });

    test("Shawn rene Horimbere", () {
      final store = SurveyStore(false);

      store.dateOfBirth = DateTime(2004, 12, 16);
      store.patientId = "LuTx3";
      store.weight = 36.0;
      store.height = 162;
      expect(store.bodySurfaceArea, 1.27);
      expect(store.bodyMassIndex, 13.7);

      store.sex = Sex.male;
      store.race = Race.white;
      store.country = "Germany";
      store.phClassification = PhClassification.HPAH;

      store.dateOfClinicalPresentation = DateTime(2015, 08, 12);
      store.clinicalEvidenceOfRvFailure = true;
      store.progressionOfSymptoms = true;
      store.syncopeInTheLast6Months = false;
      expect(store.growth, Growth.failureToThrive);
      store.whoFunctionalClass = WHOFunctionalClass.III;

      store.dateOfBloodExamination = DateTime(2015, 08, 25);
      store.serumNTproBNP = 1482;
      store.dateOfEchoCardiography = DateTime(2019, 08, 19);
      store.rarvEnlargement = RARVEnlargement.severe;
      store.rvSystolivDysfunction = RVSystolicDysfunction.dysfunction;
      store.rvlvEndsystolicRatio = 2.7;
      store.tapse = 1.56;
      expect(store.zScore, -3.69);
      expect(store.tapseLevel, TAPSELevel.belowMinus3);
      store.sdRatio = 2.6;
      store.paat = 55;
      store.pericardialEffusion = PericardialEffusion.pericardialEffusion;

      store.cardiacCatheterizationAvailable = true;
      store.dateOfLastCathStudy = DateTime(2015, 07, 21);
      store.cardiacIndex = 2.3;
      store.mRAP = 17;
      store.mPAP = 65;
      store.mSAP = 59;
      final mPapDiv = store.mPAPDividedByMSAP;
      if (mPapDiv == null) throw const FormatException("mPAPDividedByMSAP shall not be null");
      expect(mPapDiv, 1.1016949152542372);
      store.acuteVasoreactivityTesting = AcuteVasoreactivityTesting.notAvailable;
      store.pvri = 24;

      // =========== Test Results =============
      store.evaluateBlocks();
      // non invasive
      expect(store.highRiskInvasiveScore?.numerator, 20);
      expect(store.highRiskInvasiveScore?.denominator, 21);
      expect(store.highRiskScore?.numerator, 14);
      expect(store.highRiskScore?.denominator, 15);

      expect(store.lowRiskInvasiveScore?.numerator, 1);
      expect(store.lowRiskInvasiveScore?.denominator, 19);
      expect(store.lowRiskScore?.numerator, 1);
      expect(store.lowRiskScore?.denominator, 14);
    });
    test("Beispiel X", () {
      final store = SurveyStore(false);

      store.dateOfBirth = DateTime(2004, 12, 16);
      store.patientId = "LuTx3";
      store.weight = 36.0;
      store.height = 162;
      expect(store.bodySurfaceArea, 1.27);
      expect(store.bodyMassIndex, 13.7);

      store.sex = Sex.male;
      store.race = Race.white;
      store.country = "Germany";
      store.phClassification = PhClassification.HPAH;

      store.dateOfClinicalPresentation = DateTime(2015, 08, 12);
      store.clinicalEvidenceOfRvFailure = true;
      store.progressionOfSymptoms = true;
      store.syncopeInTheLast6Months = false;
      expect(store.growth, Growth.failureToThrive);
      store.whoFunctionalClass = WHOFunctionalClass.III;

      store.dateOfBloodExamination = DateTime(2015, 08, 25);
      store.serumNTproBNP = 1482;
      store.dateOfEchoCardiography = DateTime(2019, 08, 19);
      store.rarvEnlargement = RARVEnlargement.severe;
      store.rvSystolivDysfunction = RVSystolicDysfunction.dysfunction;
      store.rvlvEndsystolicRatio = 2.7;
      store.tapse = 1.56;
      expect(store.zScore, -3.69);
      expect(store.tapseLevel, TAPSELevel.belowMinus3);
      store.sdRatio = null;
      store.paat = 55;
      store.pericardialEffusion = PericardialEffusion.pericardialEffusion;

      store.cardiacCatheterizationAvailable = true;
      store.dateOfLastCathStudy = DateTime(2015, 07, 21);
      store.cardiacIndex = 2.3;
      store.mRAP = 17;
      store.mPAP = 65;
      store.mSAP = 59;
      final mPapDiv = store.mPAPDividedByMSAP;
      if (mPapDiv == null) throw const FormatException("mPAPDividedByMSAP shall not be null");
      expect(mPapDiv, 1.1016949152542372);
      store.acuteVasoreactivityTesting = AcuteVasoreactivityTesting.notAvailable;
      store.pvri = 24;

      // =========== Test Results =============
      // =========== Test Results =============
      store.evaluateBlocks();
      // non invasive
      expect(store.highRiskInvasiveScore?.numerator, 19);
      expect(store.highRiskInvasiveScore?.denominator, 20);
      expect(store.highRiskScore?.numerator, 13);
      expect(store.highRiskScore?.denominator, 14);

      expect(store.lowRiskInvasiveScore?.numerator, 1);
      expect(store.lowRiskInvasiveScore?.denominator, 18);
      expect(store.lowRiskScore?.numerator, 1);
      expect(store.lowRiskScore?.denominator, 13);
    });
    test("Low Risk Only", () {
      final store = SurveyStore(false);

      store.dateOfBirth = DateTime(2009, 01, 24);
      store.dateOfClinicalPresentation = DateTime(2014, 10, 13);
      store.patientId = "LuTx3";
      store.sex = Sex.male;
      store.race = Race.white;
      store.height = 111;
      store.weight = 20;
      expect(store.bodySurfaceArea, 0.79);
      expect(store.bodyMassIndex, 16.2);
      store.country = "Germany";
      store.phClassification = PhClassification.HPAH;
      store.clinicalEvidenceOfRvFailure = false;
      store.progressionOfSymptoms = false;
      store.syncopeInTheLast6Months = false;
      expect(store.growth, Growth.normal);
      store.whoFunctionalClass = WHOFunctionalClass.I;

      store.dateOfBloodExamination = DateTime(2015, 08, 25);
      store.serumNTproBNP = 100;
      store.dateOfEchoCardiography = DateTime(2019, 08, 19);
      store.rarvEnlargement = RARVEnlargement.minimal;
      store.rvSystolivDysfunction = RVSystolicDysfunction.noDysfunction;
      store.rvlvEndsystolicRatio = 0.4;
      store.tapse = 1.7;
      expect(store.zScore, -1.27);
      expect(store.tapseLevel, TAPSELevel.normal);
      store.sdRatio = 0.5;
      store.paat = 150;
      store.pericardialEffusion = PericardialEffusion.noPericardialEffusion;

      store.cardiacCatheterizationAvailable = true;
      store.dateOfLastCathStudy = DateTime(2015, 07, 21);
      store.cardiacIndex = 4;
      store.mRAP = 5;
      store.mPAP = 1;
      store.mSAP = 4;
      final mPapDiv = store.mPAPDividedByMSAP;
      if (mPapDiv == null) throw const FormatException("mPAPDividedByMSAP shall not be null");
      expect(mPapDiv, 0.25);
      store.acuteVasoreactivityTesting = AcuteVasoreactivityTesting.positive;
      store.pvri = 5;

      // =========== Test Results =============
      store.evaluateBlocks();
      // non invasive
      expect(store.highRiskInvasiveScore?.numerator, 0);
      expect(store.highRiskInvasiveScore?.denominator, 21);
      expect(store.highRiskScore?.numerator, 0);
      expect(store.highRiskScore?.denominator, 15);

      expect(store.lowRiskInvasiveScore?.numerator, 20);
      expect(store.lowRiskInvasiveScore?.denominator, 20);
      expect(store.lowRiskScore?.numerator, 14);
      expect(store.lowRiskScore?.denominator, 14);
    });
    test("Max Risk Only", () {
      final store = SurveyStore(false);

      store.dateOfBirth = DateTime(2003, 12, 06);
      store.patientId = "207";
      store.weight = 44.0;
      store.height = 173;
      expect(store.bodySurfaceArea, 1.45);
      expect(store.bodyMassIndex, 14.7);

      store.sex = Sex.male;
      store.race = Race.white;
      store.country = "Germany";
      store.phClassification = PhClassification.IPAH;

      store.dateOfClinicalPresentation = DateTime(2019, 01, 24);
      store.clinicalEvidenceOfRvFailure = true;
      store.progressionOfSymptoms = true;
      store.syncopeInTheLast6Months = true;
      expect(store.growth, Growth.failureToThrive);
      store.whoFunctionalClass = WHOFunctionalClass.III;

      store.dateOfBloodExamination = DateTime(2015, 08, 25);
      store.serumNTproBNP = 1500;
      store.dateOfEchoCardiography = DateTime(2019, 08, 19);
      store.rarvEnlargement = RARVEnlargement.severe;
      store.rvSystolivDysfunction = RVSystolicDysfunction.dysfunction;
      store.rvlvEndsystolicRatio = 5;
      store.tapse = -6;
      expect(store.zScore, -42.17);
      expect(store.tapseLevel, TAPSELevel.belowMinus3);
      store.sdRatio = 10;
      store.paat = 20;
      store.pericardialEffusion = PericardialEffusion.pericardialEffusion;

      store.cardiacCatheterizationAvailable = true;
      store.dateOfLastCathStudy = DateTime(2015, 07, 21);
      store.cardiacIndex = 1;
      store.mRAP = 30;
      store.mPAP = 12;
      store.mSAP = 4;
      final mPapDiv = store.mPAPDividedByMSAP;
      if (mPapDiv == null) throw const FormatException("mPAPDividedByMSAP shall not be null");
      expect(mPapDiv, 3);
      store.acuteVasoreactivityTesting = AcuteVasoreactivityTesting.negative;
      store.pvri = 25;

      // =========== Test Results =============
      store.evaluateBlocks();
      // non invasive
      expect(store.highRiskInvasiveScore?.numerator, 21);
      expect(store.highRiskInvasiveScore?.denominator, 21);
      expect(store.highRiskScore?.numerator, 15);
      expect(store.highRiskScore?.denominator, 15);

      expect(store.lowRiskInvasiveScore?.numerator, 0);
      expect(store.lowRiskInvasiveScore?.denominator, 20);
      expect(store.lowRiskScore?.numerator, 0);
      expect(store.lowRiskScore?.denominator, 14);
    });
  });
}
