import 'dart:io';

Future<void> run(String exe, String args, [String? workingDir]) async {
  final p = await Process.start(
    exe,
    args.split(" "),
    runInShell: true,
    workingDirectory: workingDir,
  );
  if (workingDir != null) {
    print("$workingDir/$exe $args");
  } else {
    print("$exe $args");
  }

  await p.stdout
    .forEach((l) => print(String.fromCharCodes(l)));
  await p.stderr
    .forEach((l) => print(String.fromCharCodes(l)));
}

void main() async {
  await run("fvm", "flutter build web -t lib/main.dart");
  await run("tar", "-czf web_standard.tar.gz web", "build");
  await run("fvm", "flutter build web -t lib/main_infant.dart");
  await run("tar", "-czf web_infant.tar.gz web", "build");
}
