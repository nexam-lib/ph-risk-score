import 'package:mobx/mobx.dart';
import 'package:phindex/models/enums.dart';
import 'package:phindex/models/fraction.dart';

part 'survey_result_store.g.dart';

class SurveyScores {
  const SurveyScores({
    required this.lowRisk,
    required this.highRisk,
    required this.lowRiskInvasive,
    required this.highRiskInvasive,
    required this.riskMode,
  });

  final Fraction lowRisk;
  final Fraction highRisk;
  final Fraction? lowRiskInvasive;
  final Fraction? highRiskInvasive;
  final RiskMode riskMode;

  bool get hasInvasiveResults => lowRiskInvasive != null && highRiskInvasive != null;

  String toString() =>
    "lowRisk:          $lowRisk\n"
    "highRisk:         $highRisk\n"
    "lowRiskInvasive:  $lowRiskInvasive\n"
    "highRiskInvasive: $highRiskInvasive\n"
    "riskMode:         $riskMode";

  bool operator==(Object other) {
    if (other is! SurveyScores)
      return false;
    
    return lowRisk == other.lowRisk &&
           highRisk == other.highRisk &&
           lowRiskInvasive == other.lowRiskInvasive &&
           highRiskInvasive == other.highRiskInvasive &&
           riskMode == other.riskMode;
  }

  int get hashCode => Object.hash(lowRisk, highRisk, lowRiskInvasive, highRiskInvasive, riskMode);
}

class Risks {
  Risks({
    Fraction? lowRisk,
    Fraction? highRisk,
  })  : lowRisk = lowRisk ?? Fraction(0, 0),
        highRisk = highRisk ?? Fraction(0, 0);

  final Fraction lowRisk;
  final Fraction highRisk;

  bool get isLowRisk => lowRisk.numerator > highRisk.numerator;
  bool get isHighRisk => highRisk.numerator > lowRisk.numerator;

  void incrementBy(Risks other) {
    lowRisk.numerator += other.lowRisk.numerator;
    lowRisk.denominator += other.lowRisk.denominator;
    highRisk.numerator += other.highRisk.numerator;
    highRisk.denominator += other.highRisk.denominator;
  }
}

class SurveyResultStore = _SurveyResultStore with _$SurveyResultStore;

abstract class _SurveyResultStore with Store {
  _SurveyResultStore(this.infantScore);

  final bool infantScore;

  // Clinical presentation
  @observable Risks? clinicalEvidenceOfRvFailure;
  @observable Risks? progressionOfSymptoms;
  @observable Risks? syncopeInLast6Months;
  @observable Risks? growth;
  @observable Risks? whoFunctionalClass;

  // Laboratory results
  @observable bool dateOfBloodExaminationValid = true;
  @observable Risks? serumNTproBNP;

  // Cardiovascular imaging
  @observable bool dateOfEchocardiographyValid = true;
  @observable Risks? rarvEnlargement;
  @observable Risks? rvSystolicDysfunction;
  @observable Risks? rvlvEndsystolicRatio;
  @observable Risks? tapse;
  @observable Risks? sdRatio;
  @observable Risks? paat;
  @observable Risks? pericardialEffusion;

  // Cardiac Catheterization
  @observable bool? cardiacCatheterizationAvailable;
  @observable bool dateOfLastCathStudyValid = true;
  @observable Risks? cardiacIndex;
  @observable Risks? mRAP;
  @observable Risks? mPAPmSAP;
  @observable Risks? acuteVasoreactivityTesting;
  @observable Risks? pvri;

  List<Risks?> get nonInvasiveRisks => [
    clinicalEvidenceOfRvFailure,
    progressionOfSymptoms,
    syncopeInLast6Months,
    growth,
    whoFunctionalClass,
    serumNTproBNP,
    rarvEnlargement,
    rvSystolicDysfunction,
    rvlvEndsystolicRatio,
    tapse,
    sdRatio,
    paat,
    pericardialEffusion,
  ];

  List<Risks?> get infantNonInvasiveRisks => [
    clinicalEvidenceOfRvFailure,
    progressionOfSymptoms,
    syncopeInLast6Months,
    growth,
    whoFunctionalClass,
    rarvEnlargement,
    rvSystolicDysfunction,
    rvlvEndsystolicRatio,
    tapse,
    sdRatio,
    pericardialEffusion,
  ];

  List<Risks?> get invasiveRisks => [
    cardiacIndex,
    mRAP,
    mPAPmSAP,
    acuteVasoreactivityTesting,
    pvri,
  ];

  List<Risks?> get nonStarredNonInvasiveRisks => [
    clinicalEvidenceOfRvFailure,
    progressionOfSymptoms,
    syncopeInLast6Months,
    growth,
    rarvEnlargement,
    rvSystolicDysfunction,
    rvlvEndsystolicRatio,
    tapse,
    sdRatio,
    paat,
    pericardialEffusion,
  ];

  List<Risks?> get starredNonInvasiveRisks => [
    whoFunctionalClass,
    serumNTproBNP
  ];

  List<Risks?> get nonStarredInvasiveRisks => [
    mPAPmSAP,
    acuteVasoreactivityTesting,
    pvri,
  ];

  List<Risks?> get starredInvasiveRisks => [
    cardiacIndex,
    mRAP,
  ];
  
  /// Returns [Risks] for the given [option].
  /// 
  /// Null is considered an invalid value, true is considered a highRisk value and false
  /// is considered a lowRisk value.
  /// [weight] (usually 1 or 2) controls the value of the denominators and the numerator,
  /// if any.
  Risks? evaluateOption(bool? option, {int weight = 1}) {
    if (option == null)
      return null;
    else
      return option
        ? Risks(lowRisk: Fraction(0, weight), highRisk: Fraction(weight, weight))
        : Risks(lowRisk: Fraction(weight, weight), highRisk: Fraction(0, weight));
  }

  /// Returns [Risks] for the given [option].
  /// 
  /// Null is considered an invalid value, the first enum value is considered lowRisk,
  /// the second one is considered neutral and the third one is considered highRisk.
  /// [weight] (usually 1 or 2) controls the value of the denominators and the numerator,
  /// if any.
  Risks? evaluateTripleOption<E extends Enum>(E? option, {int weight = 1}) {
    if (option == null)
      return null;
    else
      switch (option.index) {
        case 0: return Risks(lowRisk: Fraction(weight, weight), highRisk: Fraction(0, weight));
        case 1: return Risks(lowRisk: Fraction(0, weight), highRisk: Fraction(0, weight));
        case 2: return Risks(lowRisk: Fraction(0, weight), highRisk: Fraction(weight, weight));
        default: throw ArgumentError("Only enums with three values (in increasing severity) accepted.");
      }
  }

  @action void evaluateClinicalEvidenceOfRVFailure(bool? hasEvidenceOfRvFailure) =>
    clinicalEvidenceOfRvFailure = evaluateOption(hasEvidenceOfRvFailure);

  @action void evaluateProgressionOfSymptoms(bool? hasProgressionOfSymptoms) =>
    progressionOfSymptoms = evaluateOption(hasProgressionOfSymptoms);

  @action void evaluateSyncopeInLast6Months(bool? hasSyncopeInLast6Months) =>
    syncopeInLast6Months = evaluateOption(hasSyncopeInLast6Months);

  @action void evaluateGrowth(Growth? level) =>
    growth = evaluateTripleOption(level);

  @action void evaluateWhoFunctionalClass(WHOFunctionalClass? who) {
    switch (who) {
      case null:
        whoFunctionalClass = null;
        break;
      case WHOFunctionalClass.I:
      case WHOFunctionalClass.II:
        whoFunctionalClass = Risks(lowRisk: Fraction(2, 2), highRisk: Fraction(0, 2));
        break;
      case WHOFunctionalClass.III:
      case WHOFunctionalClass.IV:
        whoFunctionalClass = Risks(lowRisk: Fraction(0, 2), highRisk: Fraction(2, 2));
        break;
    }
  }

  @action void evaluateSerumNTproBNPLevel(double? age, SerumNTproBNP? level) {
    if (age == null)
      serumNTproBNP = null;
    else if (age <= 1)
      serumNTproBNP = Risks();
    else
      serumNTproBNP = evaluateTripleOption(level, weight: 2);
  }

  @action void evaluateRARVEnlargement(RARVEnlargement? level) =>
    rarvEnlargement = evaluateTripleOption(level);

  @action void evaluateRVSystolicDysfunction(RVSystolicDysfunction? level) =>
    rvSystolicDysfunction = evaluateTripleOption(level);

  @action void evaluateRVLVEndsystolicRatio(RVLVEndsystolicRatio? level) =>
    rvlvEndsystolicRatio = evaluateTripleOption(level);

  @action void evaluateTAPSE(TAPSELevel? level) =>
    tapse = evaluateTripleOption(level);

  @action void evaluateSDRatio(bool available, SDRatioLevel? level) {
    if (!available)
      sdRatio = Risks();
    else
      sdRatio = evaluateTripleOption(level);
  }

  @action void evaluatePAAT(bool available, double? age, PAATLevel? level) {
    if (!available)
      paat = Risks();
    else if (age == null)
      paat = null;
    else if (age <= 1)
      paat = Risks();
    else
      paat = evaluateTripleOption(level);
  }

  @action void evaluatePericardialEffusion(PericardialEffusion? effusion) {
    switch (effusion) {
      case null:
        pericardialEffusion = null;
        break;
      case PericardialEffusion.noPericardialEffusion:
        pericardialEffusion = Risks(highRisk: Fraction(1, 1));
        break;
      case PericardialEffusion.pericardialEffusion:
        pericardialEffusion = Risks(highRisk: Fraction(0, 1));
        break;
    }
  }

  @action void evaluateCardiacIndex(CardiacIndexLevel? level) =>
    cardiacIndex = evaluateTripleOption(level, weight: 2);

  @action void evaluateMRAP(MRAPLevel? level) =>
    mRAP = evaluateTripleOption(level, weight: 2);

  @action void evaluateMPAPmSAP(MPAPmSAPLevel? level) =>
    mPAPmSAP = evaluateTripleOption(level);

  @action void evaluateAcuteVasoreactivityTesting(AcuteVasoreactivityTesting? testing) {
    switch (testing) {
      case AcuteVasoreactivityTesting.positive:
        acuteVasoreactivityTesting = Risks(lowRisk: Fraction(1, 1));
        break;
      case AcuteVasoreactivityTesting.negative:
        acuteVasoreactivityTesting = Risks(lowRisk: Fraction(0, 1));
        break;
      case null:
      case AcuteVasoreactivityTesting.notAvailable:
        acuteVasoreactivityTesting = Risks();
        break;
    }
  }

  @action void evaluatePVRi(double? p, bool? pvriAbove15WUxMSquared) {
    if (pvriAbove15WUxMSquared == null)
      pvri = null;
    else
      pvri = Risks(highRisk: Fraction(pvriAbove15WUxMSquared ? 1 : 0, 1));
  }

  @computed SurveyScores? get determineScores {
    final cardiacCatheterizationAvailable = this.cardiacCatheterizationAvailable;
    if (!dateOfBloodExaminationValid || !dateOfEchocardiographyValid || cardiacCatheterizationAvailable == null)
      return null;

    final nonInvasiveRiskResults = Risks();

    final risks = infantScore ? infantNonInvasiveRisks : nonInvasiveRisks;
    for (final risks in risks) {
      if (risks == null)
        return null;
      else
        nonInvasiveRiskResults.incrementBy(risks);
    }

    Risks? invasiveRiskResults;
    if (cardiacCatheterizationAvailable) {
      if (!dateOfLastCathStudyValid)
        return null;

      invasiveRiskResults = Risks(
        lowRisk: nonInvasiveRiskResults.lowRisk.copyWith(),
        highRisk: nonInvasiveRiskResults.highRisk.copyWith(),
      );
      for (final risks in invasiveRisks) {
        if (risks == null) {
          return null;
        } else {
          invasiveRiskResults.incrementBy(risks);
        }
      }
    } else {
      invasiveRiskResults = null;
    }

    int nonStarredLowRisk = 0;
    int nonStarredHighRisk = 0;
    int starredLowRisk = 0;
    int starredHighRisk = 0;
    final nonStarredRisks = [
      ...nonStarredNonInvasiveRisks,
      if (cardiacCatheterizationAvailable)
        ...nonStarredInvasiveRisks,
    ];
    for (final risks in nonStarredRisks) {
      if (risks != null) {
        if (risks.isLowRisk)
          ++nonStarredLowRisk;
        else if (risks.isHighRisk)
          ++nonStarredHighRisk;
      }
    }

    final starredRisks = [
      ...starredNonInvasiveRisks,
      if (cardiacCatheterizationAvailable)
        ...starredInvasiveRisks,
    ];
    for (final risks in starredRisks) {
      if (risks != null) {
        if (risks.isLowRisk)
          ++starredLowRisk;
        else if (risks.isHighRisk)
          ++starredHighRisk;
      }
    }
    
    final RiskMode riskMode;
    if (starredLowRisk >= 3 && starredHighRisk == 0 && nonStarredHighRisk == 0)
      riskMode = RiskMode.low;
    else if (nonStarredLowRisk >= 5 && starredHighRisk == 0 && nonStarredLowRisk == 0)
      riskMode = RiskMode.low;
    else if (starredHighRisk >= 2 && (cardiacIndex?.isHighRisk ?? false))
      riskMode = RiskMode.high;
    else if ((serumNTproBNP?.isHighRisk ?? false) && nonStarredHighRisk >= 5)
      riskMode = RiskMode.high;
    else
      riskMode = RiskMode.intermediate;
    
    return SurveyScores(
      lowRisk: nonInvasiveRiskResults.lowRisk,
      highRisk: nonInvasiveRiskResults.highRisk,
      lowRiskInvasive: invasiveRiskResults?.lowRisk,
      highRiskInvasive: invasiveRiskResults?.highRisk,
      riskMode: riskMode,
    );
  }
}
