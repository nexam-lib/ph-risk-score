class Fraction {
  Fraction(this.numerator, this.denominator);

  int numerator;
  int denominator;

  double toDouble() => numerator / denominator;

  Fraction copyWith({
    int? numerator,
    int? denominator,
  }) => Fraction(
    numerator ?? this.numerator,
    denominator ?? this.denominator,
  );

  String toString() => "$numerator/$denominator";

  bool operator==(Object other) {
    if (other is! Fraction)
      return false;
    
    return numerator == other.numerator && denominator == other.denominator;
  }

  int get hashCode => Object.hash(numerator, denominator);
}
