import 'package:phindex/models/fraction.dart';
import 'package:phindex/util/util.dart';

class AgePercentile {
  const AgePercentile(this.age, this.p3, this.p10);

  final double age;
  final double p3;
  final double p10;
}

class AgeMeanPercentile {
  const AgeMeanPercentile(this.age, this.mean, this.sd);

  final double age;
  final double mean;
  final double sd;
}

class RiskState {
  final lowRisk = Fraction(0, 0);
  final highRisk = Fraction(0, 0);
  var cathLowRisk = Fraction(0, 0);
  var cathHighRisk = Fraction(0, 0);

  void addCathRiskFor(String name, {bool? isHighRisk, int value = 1}) {
    log("Update cath risk score with (Name: $name / Value: $value");
    log("Values before:${toString()}");
    if (isHighRisk != null) {
      if (isHighRisk) {
        cathHighRisk.numerator += value;
      } else {
        cathLowRisk.numerator += value;
      }
    }
    cathLowRisk.denominator += value;
    cathHighRisk.denominator += value;
    log("Values after:${toString()}");
  }

  void addRiskFor(String name, {bool? isHighRisk, int value = 1}) {
    log("Update risk score with (Name: $name / Value: $value");
    log("Values before:${toString()}");
    if (isHighRisk != null) {
      if (isHighRisk) {
        highRisk.numerator += value;
      } else {
        lowRisk.numerator += value;
      }
    }
    lowRisk.denominator += value;
    highRisk.denominator += value;
    log("Values after:${toString()}");
  }

  @override
  String toString() {
    return "NonInvasive:\nLow Risk: (${lowRisk.numerator} '/' ${lowRisk.denominator})\nHigh Risk: (${highRisk.numerator} '/' ${highRisk.denominator})\n"
        "\nInvasive:\nLow Risk:(${cathLowRisk.numerator} '/' ${cathLowRisk.denominator})\nHigh Risk: (${cathHighRisk.numerator} '/' ${cathHighRisk.denominator}) ========\n\n";
  }

  void addValueSeparated(String name, bool? addLowRisk, [int denom = 1, int num = 1]) {
    if (addLowRisk == null) throw ArgumentError("$name not set");
    addLowRisk ? lowRisk.denominator += denom : highRisk.denominator += denom;
    addLowRisk ? lowRisk.numerator += num : highRisk.numerator += num;
  }
}
