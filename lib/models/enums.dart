enum TripleOption { yes, no, notAvailable }

enum RiskMode { low, intermediate, high }

enum Sex { female, male, diverse }
extension SexExtension on Sex {
  bool get diverse => this == Sex.male || this == Sex.diverse;
}

enum Race { asian, black, freetext, hispanic, white, other }

enum Continent {
  africa,
  antarctica,
  asia,
  europe,
  northAmerica,
  southAmerica,
  oceania,
}

enum PhClassification {
  /// 1.1
  IPAH,
  /// 1.2
  HPAH,
  /// 1.4.4
  PAH_CHD,
  /// 2
  PH_LHD,
  /// 3
  PH_CLD,
  /// 4
  CTEPH,
  /// 5
  unclear,
  /// 5.4
  PH_ComplexCHD,
}

enum Growth { normal, slightlyRestricted, failureToThrive }

enum WHOFunctionalClass { I, II, III, IV }

enum SerumNTproBNP { minimallyElevated, moderatedlyElevated, greatlyElevated }

enum RARVEnlargement { minimal, moderate, severe }

enum RVSystolicDysfunction { noDysfunction, moderateDysfunction, dysfunction }

enum RVLVEndsystolicRatio { below1, between1And1Point5, severe }

enum TAPSELevel { normal, moderateLower, belowMinus3 }

enum SDRatioLevel { below1, between1And1point4, above1Point4 }

enum PAATLevel { over100, between70And100, below70 }

enum PericardialEffusion { pericardialEffusion, noPericardialEffusion }

enum CardiacIndexLevel { over3, between3And2Point5, below2Point5 }

enum MRAPLevel { below10, between10And15, over15 }

enum MPAPmSAPLevel { below0Point5, between0Point5And0Point75, above0Point75 }

enum AcuteVasoreactivityTesting { positive, negative, notAvailable }
