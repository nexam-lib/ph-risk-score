enum Mode {
  standard,
  infant,
}

class AppOptions {
  const AppOptions({
    required this.mode,
  });

  final Mode mode;
}
