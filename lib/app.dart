import 'package:flutter/material.dart';
import 'package:phindex/models/app_options.dart';
import 'package:phindex/pages/survey_page/survey_page.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:phindex/util/theme.dart';
import 'package:provider/provider.dart';

class App extends StatelessWidget {
  const App({
    Key? key,
    required this.options,
  }) : super(key: key);

  final AppOptions options;

  Widget build(BuildContext context) => MultiProvider(
    providers: [
      Provider<FormTheme>(
        create: (_) => const FormTheme(
          maxWidth: 600.0,
          containerPadding: EdgeInsetsDirectional.all(0.0),
          containerMargin: EdgeInsetsDirectional.fromSTEB(0.0, 4.0, 0.0, 4.0),
          textFieldPadding: EdgeInsetsDirectional.fromSTEB(12.0, 8.0, 12.0, 8.0),
          fieldTitleGap: 12.0,
          nestedTextFieldPadding: EdgeInsetsDirectional.fromSTEB(12.0, 8.0, 8.0, 8.0),
          customChildrenGap: 10.0,
          fieldPadding: EdgeInsetsDirectional.fromSTEB(12.0, 8.0, 12.0, 8.0),
        ),  
      ),
      Provider<AppOptions>.value(
        value: options,
      ),
    ],
    child: MaterialApp(
      title: "EPPVDN PH Risk Score",
      theme: ThemeData(
        primarySwatch: Colors.red,
        dividerColor: const Color(0xFF79747E),
        dividerTheme: const DividerThemeData(
          color: Color(0xFF79747E),
          thickness: 1.0,
          indent: 12.0,
          endIndent: 12.0,
          space: 0.0,
        ),
        inputDecorationTheme: const InputDecorationTheme(
          floatingLabelBehavior: FloatingLabelBehavior.always,
          isDense: true,
          contentPadding: EdgeInsets.symmetric(
            horizontal: 12.0,
            vertical: 14.0,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
            borderSide: BorderSide(
              color: Color(0xFF79747E),
              style: BorderStyle.solid,
              width: 1.0,
            ),
          ),
        ),
        checkboxTheme: const CheckboxThemeData(
          visualDensity: VisualDensity.compact,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(
            color: grey,
            width: 2.0,
            style: BorderStyle.solid,
          ),
        ),
      ),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: const SurveyPage(),
    ),
  );
}
