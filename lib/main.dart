import 'package:flutter/widgets.dart';
import 'package:phindex/app.dart';
import 'package:phindex/models/app_options.dart';

void main() {
  runApp(const App(
    options: AppOptions(
      mode: Mode.standard,
    ),
  ));
}
