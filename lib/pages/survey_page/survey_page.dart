import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide FormField;
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart' hide FormField;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:phindex/models/app_options.dart';

import 'package:phindex/models/enums.dart';
import 'package:phindex/models/fraction.dart';
import 'package:phindex/models/survey_result_store.dart';
import 'package:phindex/pages/survey_page/survey_page_store.dart';
import 'package:phindex/util/app_localizations_extension.dart';
import 'package:phindex/util/theme.dart';
import 'package:phindex/util/util.dart';
import 'package:phindex/widgets/checkbox_tile.dart';
import 'package:phindex/widgets/checkboxes.dart';
import 'package:phindex/widgets/clickable_text.dart';
import 'package:phindex/widgets/custom_triple_checkbox.dart';
import 'package:phindex/widgets/date_picker.dart';
import 'package:phindex/widgets/dropdown.dart';
import 'package:phindex/widgets/enabled.dart';
import 'package:phindex/widgets/form_container.dart';
import 'package:phindex/widgets/form_field.dart';
import 'package:phindex/widgets/required_box.dart';
import 'package:provider/provider.dart';

class SurveyPage extends StatefulWidget {
  const SurveyPage({Key? key}) : super(key: key);

  _SurveyPageState createState() => _SurveyPageState();
}

class _SurveyPageState extends State<SurveyPage> {
  late AppLocalizations l;
  late final SurveyStore store;

  void initState() {
    super.initState();
    store = SurveyStore(context.read<AppOptions>().mode == Mode.infant);
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    l = context.localization;
  }

  void dispose() {
    store.dispose();
    super.dispose();
  }

  Widget buildSubtitle(String label, FormTheme formTheme) => Padding(
        padding: EdgeInsetsDirectional.only(start: formTheme.fieldPadding.start),
        child: SelectableText(label, style: subtitle),
      );

  Widget buildPatientData(BuildContext context, FormTheme formTheme) => FormContainer(
        child: Column(
          children: [
            FormField(
              label: l.surnameFirstname,
              hintText: l.surnameFirstnameHint,
              onChanged: (fullName) => store.fullName = fullName,
              keyboardType: TextInputType.name,
              textInputAction: TextInputAction.next,
            ),
            DatePicker(
              label: l.dateOfBirth,
              required: true,
              onChanged: (dateOfBirth) => store.dateOfBirth = dateOfBirth,
            ),
            FormField(
              label: l.patientsID,
              hintText: l.patientsIdHint,
              onChanged: (patientId) => store.patientId = patientId,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
            ),
            FormField(
              label: l.weightInKg,
              hintText: l.weightHint,
              required: true,
              inputFormatters: [DoubleInputFormatter(validator: (w) => w > 0.0)],
              onChanged: (weight) => store.weight = tryParseDouble(weight),
              keyboardType: const TextInputType.numberWithOptions(decimal: true),
            ),
            FormField(
              label: l.heightInCm,
              hintText: l.heightHint,
              required: true,
              inputFormatters: [DoubleInputFormatter(validator: (h) => h > 0.0)],
              onChanged: (height) => store.height = tryParseDouble(height),
              keyboardType: const TextInputType.numberWithOptions(decimal: true),
            ),
            Dropdown<Sex>(
              label: l.sex,
              hintText: l.sexHint,
              values: Sex.values,
              localize: l.translateSex,
              required: true,
              onChanged: (sex) {
                if (sex != null) {
                  store.sex = sex;
                }
              },
            ),
            Observer(
              builder: (context) => FormField(
                label: l.bsaInSquareMeters,
                hintText: l.bsaHintText,
                value: store.bodySurfaceArea.map((bsa) => bsa.toStringAsFixed(2)),
                readOnly: true,
              ),
            ),
            Observer(
              builder: (context) => FormField(
                label: l.bmiInKgPerSquareMeter,
                hintText: l.bmiHintText,
                value: store.bodyMassIndex.map((bmi) => bmi.toStringAsFixed(2)),
                readOnly: true,
              ),
            ),
            Dropdown<Race>(
              label: l.raceEthnicity,
              hintText: l.raceEthnicityHint,
              values: Race.values,
              localize: l.translateRace,
              onChanged: (race) => store.race = race,
            ),
            Observer(
              builder: (context) => FormField(
                label: l.otherRaceEthnicity,
                hintText: l.otherRaceEthnicityHintText,
                onChanged: (race) => store.freeTextRace = race,
                readOnly: store.race != Race.freetext,
              ),
            ),
            FormField(
              label: l.country,
              hintText: l.countryHint,
              onChanged: (country) => store.country = country,
              autocorrect: true,
              keyboardType: TextInputType.name,
            ),
            Dropdown<Continent>(
              label: l.continent,
              hintText: l.continentHint,
              values: Continent.values,
              localize: l.translateContinent,
              onChanged: (continent) => store.continent = continent,
            ),
            FormField(
              label: l.phCenter,
              hintText: l.phCenterHint,
              onChanged: (phCenter) => store.fullName = phCenter,
              autocorrect: true,
              keyboardType: TextInputType.name,
            ),
            Dropdown<PhClassification>(
              label: l.mainPhClassification,
              hintText: l.mainPhClassificationHint,
              values: PhClassification.values,
              localize: l.translatePhClassification,
              onChanged: (phClassification) => store.phClassification = phClassification,
            ),
            Dropdown<PhClassification>(
              label: l.otherPhClassification,
              hintText: l.otherPhClassificationHint,
              values: PhClassification.values,
              localize: l.translatePhClassification,
              onChanged: (otherPhClassification) =>
                  store.otherPhClassification = otherPhClassification,
            ),
            FormField(
              label: l.diagnosis,
              hintText: l.diagnosisHint,
              onChanged: (diagnosis) => store.otherPhClassificationDiagnosis = diagnosis,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              minLines: 2,
              maxLines: null,
            ),
          ],
        ),
      );

  Widget buildClinicalPresentation(BuildContext context, FormTheme formTheme) =>
      FormContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DatePicker(
              label: l.dateOfClinicalPresentation,
              required: true,
              onChanged: (dateOfClinicalPresentation) =>
                  store.dateOfClinicalPresentation = dateOfClinicalPresentation,
            ),
            const Divider(),
            CheckboxTile.greenRed(
              label: l.clinicalEvidenceOfRVFailure,
              required: true,
              proportions: Fraction(8, 3),
              description: SelectableText(
                l.clinicalEvidenceOfRVFailureDescription,
                style: description,
              ),
              onChanged: (evidence) => store.clinicalEvidenceOfRvFailure = evidence,
            ),
            const Divider(),
            CheckboxTile.greenRed(
              label: l.progressionOfSymptoms,
              required: true,
              onChanged: (progression) => store.progressionOfSymptoms = progression,
            ),
            const Divider(),
            CheckboxTile.greenRed(
              label: l.syncopeInTheLastSixMonths,
              required: true,
              onChanged: (syncope) => store.syncopeInTheLast6Months = syncope,
            ),
            const Divider(),
            Observer(
              builder: (context) => CustomTripleCheckbox<Growth>(
                label: l.growth,
                localize: l.translateGrowth,
                values: Growth.values,
                selectedValue: store.growth,
                proportions: Fraction(1, 1),
                labelAlign: TextAlign.end,
                childrenPadding: const EdgeInsetsDirectional.only(start: 10.0),
                children: [
                  SelectableText(
                    l.growthHelperText,
                    style: formSubtitle.copyWith(color: grey),
                  ),
                  SelectableText(
                    "${l.bmiPercentile}: ${l.translatePercentile(store.bodyMassIndex, store.bmiPercentile)}",
                    style: formSubtitle,
                  ),
                  SelectableText(
                    "${l.heightPercentile}: ${l.translatePercentile(store.height, store.heightPercentile)}",
                    style: formSubtitle,
                  ),
                ],
              ),
            ),
            const Divider(),
            Padding(
              padding: formTheme.fieldPadding.copyWith(bottom: formTheme.fieldTitleGap),
              child: RequiredBox(
                required: true,
                child: SelectableText(
                  l.whoFunctionalClass,
                  style: title,
                ),
              ),
            ),
            Dropdown<WHOFunctionalClass>(
              padding: formTheme.fieldPadding.copyWith(top: 0.0),
              label: l.whoFunctionalClassInput,
              hintText: l.whoFunctionalClassHint,
              values: WHOFunctionalClass.values,
              localize: l.translateWHOFunctionalClass,
              onChanged: (whoFunctionalClass) =>
                  store.whoFunctionalClass = whoFunctionalClass,
            ),
            Padding(
              padding: formTheme.fieldPadding.copyWith(top: 0.0),
              child: RichText(
                text: TextSpan(
                  style: formSubtitle,
                  children: [
                    TextSpan(text: l.whoFunctionalClassDescription),
                    WidgetSpan(
                      child: ClickableText(
                        text: l.whoFunctionalClassVisibleLink,
                        style: formSubtitle.copyWith(
                          decoration: TextDecoration.underline,
                        ),
                        onPressed: () => tryLaunch(l.whoFunctionalClassLink),
                      ),
                    ),
                    TextSpan(text: l.whoFunctionalClassTable),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

  Widget buildLaboratoryResults(BuildContext context, FormTheme formTheme) =>
      FormContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: formTheme.fieldPadding.copyWith(bottom: 0.0),
              child: RequiredBox(
                required: true,
                child: SelectableText(
                  l.serumNTproBNP,
                  style: title,
                ),
              ),
            ),
            Observer(
              builder: (context) => DatePicker(
                label: l.dateOfBloodExamination,
                onChanged: (dateOfBloodExamination) =>
                    store.dateOfBloodExamination = dateOfBloodExamination,
                errorText: () {
                  if (store.moreThan1YearBetweenClinicalPresentationAndBloodExamination)
                    return l.dateOfBloodExaminationErrorText;
                  else if (store.atLeast1YearOldAtDateOfBloodExamination)
                    return l.serumNTproBNPErrorText;
                  else
                    return null;
                }(),
              ),
            ),
            Observer(
              builder: (context) => CustomTripleCheckbox<SerumNTproBNP>(
                padding: formTheme.fieldPadding.copyWith(top: 0.0),
                selectedValue: store.serumNTproBNPLevel,
                values: SerumNTproBNP.values,
                localize: l.translateSerumNTproBNP,
                labelAlign: TextAlign.justify,
                children: [
                  Observer(
                    builder: (context) {
                      final moreThan1YearOld = store.moreThan1YearOld;
                      final examinationNotUpToDate = store
                          .moreThan1YearBetweenClinicalPresentationAndBloodExamination;
                      return FormField(
                        padding: EdgeInsets.zero,
                        label: l.serumNTproBNPInput,
                        helperText: l.serumNTproBNPHelperText,
                        inputFormatters: [DoubleInputFormatter(validator: (d) => d > 0)],
                        readOnly: !moreThan1YearOld || examinationNotUpToDate,
                        onChanged: (serum) {
                          store.serumNTproBNP = tryParseDouble(serum);
                          if (store.serumNTproBNP == null)
                            store.rapidlyRisingNTProBNPLevel = false;
                        },
                        keyboardType: const TextInputType.numberWithOptions(),
                      );
                    },
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Expanded(
                        child: SelectableText(
                          l.serumNTproBNPRapidlyRising,
                          style: formSubtitle.copyWith(color: grey),
                        ),
                      ),
                      Observer(
                        builder: (context) => Checkbox(
                          activeColor: grey,
                          value: store.rapidlyRisingNTProBNPLevel,
                          onChanged:
                              store.serumNTproBNP == null || !store.moreThan1YearOld
                                  ? null
                                  : (rapidlyRising) => store.rapidlyRisingNTProBNPLevel =
                                      rapidlyRising ?? false,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  Widget buildMedicalImaging(BuildContext context, FormTheme formTheme) => FormContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Observer(
              builder: (context) => DatePicker(
                label: l.dateOfEchocardiography,
                onChanged: (dateOfEchocardiography) =>
                    store.dateOfEchoCardiography = dateOfEchocardiography,
                required: true,
                errorText:
                    store.moreThan1YearBetweenClinicalPresentationAndEchocardiography
                        ? l.dateOfEchocardiographyErrorText
                        : null,
              ),
            ),
            buildSubtitle(l.echocardiography, formTheme),
            Observer(
              builder: (context) => CustomTripleCheckbox<RARVEnlargement>(
                label: l.raRVEnlargement,
                selectedValue: store.rarvEnlargement,
                required: true,
                onChanged: (rarv) {
                  if (rarv != null) store.rarvEnlargement = rarv;
                },
                values: RARVEnlargement.values,
                localize: l.translateRARVEnlarement,
              ),
            ),
            const Divider(),
            Observer(
              builder: (context) => CustomTripleCheckbox<RVSystolicDysfunction>(
                label: l.rvSystolicDysfunction,
                selectedValue: store.rvSystolivDysfunction,
                required: true,
                onChanged: (rv) {
                  if (rv != null) store.rvSystolivDysfunction = rv;
                },
                values: RVSystolicDysfunction.values,
                localize: l.translateRVSystolicDysfunction,
              ),
            ),
            const Divider(),
            Observer(
              builder: (context) => CustomTripleCheckbox<RVLVEndsystolicRatio>(
                label: l.rvLVEndsystolicRatio,
                selectedValue: store.rvlvEndsystolicRatioLevel,
                required: true,
                values: RVLVEndsystolicRatio.values,
                localize: l.translateRVLVEndsystolicRatio,
                children: [
                  FormField(
                    label: l.rvLVEndsystolicRatioInput,
                    inputFormatters: [DoubleInputFormatter()],
                    onChanged: (ratio) =>
                        store.rvlvEndsystolicRatio = tryParseDouble(ratio),
                    padding: EdgeInsets.zero,
                    autocorrect: false,
                    keyboardType: const TextInputType.numberWithOptions(
                        decimal: true, signed: false),
                  ),
                ],
              ),
            ),
            const Divider(),
            Observer(
              builder: (context) => CustomTripleCheckbox<TAPSELevel>(
                label: l.tapse,
                selectedValue: store.tapseLevel,
                required: true,
                values: TAPSELevel.values,
                localize: l.translateTAPSE,
                children: [
                  FormField(
                    label: l.tapseInput,
                    inputFormatters: [DoubleInputFormatter()],
                    onChanged: (tapse) => store.tapse = tryParseDouble(tapse),
                    keyboardType: const TextInputType.numberWithOptions(
                        decimal: true, signed: false),
                    textInputAction: TextInputAction.next,
                    padding: EdgeInsets.zero,
                  ),
                  SelectableText(
                    "${l.tapseZScore}: ${store.zScore ?? ""}",
                    style: formSubtitle,
                  ),
                ],
              ),
            ),
            const Divider(),
            Observer(
              builder: (context) => CustomTripleCheckbox<SDRatioLevel>(
                label: l.sdRatio,
                selectedValue: store.sdRatioLevel,
                values: SDRatioLevel.values,
                localize: l.translateSdRatio,
                children: [
                  FormField(
                    label: l.sdRatioInput,
                    inputFormatters: [DoubleInputFormatter()],
                    onChanged: (sdRatio) => store.sdRatio = tryParseDouble(sdRatio),
                    readOnly: !store.sdRatioAvailable,
                    keyboardType: const TextInputType.numberWithOptions(
                        decimal: true, signed: false),
                    padding: EdgeInsets.zero,
                  ),
                  Row(
                    children: [
                      SelectableText(
                        l.notAvailable,
                        style: formSubtitle.copyWith(color: grey),
                      ),
                      Observer(
                        builder: (context) => Checkbox(
                          activeColor: grey,
                          value: !store.sdRatioAvailable,
                          onChanged: (checked) {
                            if (checked == null) return;

                            store.sdRatioAvailable = !checked;
                            if (checked) store.sdRatio = null;
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(),
            Observer(
              builder: (context) => Enabled(
                enabled: !store.infantScore,
                child: CustomTripleCheckbox<PAATLevel>(
                  label: l.paat,
                  selectedValue: store.paatLevel,
                  values: PAATLevel.values,
                  localize: l.translatePAAT,
                  children: [
                    Observer(
                      builder: (context) {
                        final moreThan1YearOld = store.moreThan1YearOld;
                        return FormField(
                          label: l.paatInput,
                          helperText: !moreThan1YearOld ? l.paatHelperText : null,
                          readOnly: moreThan1YearOld && !store.paatAvailable,
                          inputFormatters: [
                            DoubleInputFormatter(validator: (d) => d > 0.0)
                          ],
                          onChanged: (paat) => store.paat = tryParseDouble(paat),
                          keyboardType: const TextInputType.numberWithOptions(
                              decimal: true, signed: false),
                          textInputAction: TextInputAction.next,
                          padding: EdgeInsets.zero,
                        );
                      },
                    ),
                    Row(
                      children: [
                        SelectableText(
                          l.notAvailable,
                          style: formSubtitle.copyWith(color: grey),
                        ),
                        Observer(
                          builder: (context) => Checkbox(
                            activeColor: grey,
                            value: !store.paatAvailable,
                            onChanged: (checked) {
                              if (checked == null) return;
              
                              store.paatAvailable = !checked;
                              if (checked) store.paat = null;
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            const Divider(),
            Padding(
              padding: formTheme.fieldPadding,
              child: RequiredBox(
                required: true,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 4,
                      child: SelectableText(l.pericardialEffusion, style: title),
                    ),
                    Flexible(
                      flex: 3,
                      child: Checkboxes<PericardialEffusion>(
                        onChanged: (effusion) => store.pericardialEffusion = effusion,
                        selectedValue: store.pericardialEffusion,
                        checkboxInfos: [
                          CheckboxInfo(red, l.pericardialEffusionOutputYes,
                              PericardialEffusion.noPericardialEffusion),
                          CheckboxInfo(black, l.pericardialEffusionOutputNo,
                              PericardialEffusion.pericardialEffusion),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

  Widget buildCardiacCatheterization(BuildContext context, FormTheme formTheme) =>
      FormContainer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: formTheme.fieldPadding.copyWith(bottom: 0.0, start: 0.0),
              child: buildSubtitle(l.dateOfLastCathStudy, formTheme),
            ),
            Observer(
              builder: (context) => DatePicker(
                label: l.dateOfLastCathStudyInput,
                onChanged: (lastCathStudy) => store.dateOfLastCathStudy = lastCathStudy,
                required: store.cardiacCatheterizationAvailable ?? false,
                errorText: store.moreThan1YearBetweenCathStudyAndOtherExaminations
                    ? l.dateOfLastCathStudyErrorText
                    : null,
              ),
            ),
            buildSubtitle(l.invasiveHemodynamics, formTheme),
            Observer(
              builder: (context) => CustomTripleCheckbox<CardiacIndexLevel>(
                  label: l.cardiacIndex,
                  selectedValue: store.cardiacIndexLevel,
                  required: store.cardiacCatheterizationAvailable ?? false,
                  values: CardiacIndexLevel.values,
                  localize: l.translateCardiacIndex,
                  children: [
                    FormField(
                      label: l.cardiacIndexInput,
                      inputFormatters: [DoubleInputFormatter(validator: (d) => d >= 0)],
                      onChanged: (cardiacIndex) =>
                          store.cardiacIndex = tryParseDouble(cardiacIndex),
                      keyboardType: const TextInputType.numberWithOptions(
                          decimal: true, signed: false),
                      padding: EdgeInsets.zero,
                    ),
                  ]),
            ),
            const Divider(),
            Observer(
              builder: (context) => CustomTripleCheckbox<MRAPLevel>(
                label: l.mRAP,
                selectedValue: store.mRAPLevel,
                required: store.cardiacCatheterizationAvailable ?? false,
                values: MRAPLevel.values,
                localize: l.translateMRAP,
                children: [
                  FormField(
                    label: l.mRAPInput,
                    inputFormatters: [DoubleInputFormatter(validator: (d) => d >= 0)],
                    onChanged: (mRAP) => store.mRAP = tryParseDouble(mRAP),
                    keyboardType: const TextInputType.numberWithOptions(
                        decimal: true, signed: false),
                    padding: EdgeInsets.zero,
                  ),
                ],
              ),
            ),
            const Divider(),
            Observer(
              builder: (context) => CustomTripleCheckbox<MPAPmSAPLevel>(
                label: l.mPAPmSAP,
                selectedValue: store.mPAPmSAPLevel,
                required: store.cardiacCatheterizationAvailable ?? false,
                values: MPAPmSAPLevel.values,
                localize: l.translateMPAPmSAP,
                children: [
                  FormField(
                    label: l.mPAPInput,
                    inputFormatters: [DoubleInputFormatter(validator: (d) => d >= 0)],
                    onChanged: (mPAP) => store.mPAP = int.tryParse(mPAP),
                    keyboardType: TextInputType.number,
                    padding: EdgeInsets.zero,
                  ),
                  FormField(
                    label: l.mSAPInput,
                    inputFormatters: [DoubleInputFormatter(validator: (d) => d > 0)],
                    onChanged: (mSAP) => store.mSAP = int.tryParse(mSAP),
                    keyboardType: TextInputType.number,
                    padding: EdgeInsets.zero,
                  ),
                ],
              ),
            ),
            const Divider(),
            Observer(
              builder: (context) => CustomTripleCheckbox<AcuteVasoreactivityTesting>(
                label: l.acuteVasoreactivity,
                selectedValue: store.acuteVasoreactivityTesting,
                values: AcuteVasoreactivityTesting.values,
                localize: l.translateAcuteVasoreactivityTesting,
                colors: const [green, black, grey],
                onChanged: (avt) => store.acuteVasoreactivityTesting = avt,
                labelAlign: TextAlign.end,
                proportions: Fraction(8, 3),
                children: [
                  SelectableText(
                    l.acuteVasoreactivityDescription,
                    style: formSubtitle,
                  ),
                ],
              ),
            ),
            const Divider(),
            Observer(
              builder: (context) => CheckboxTile<bool>(
                padding: formTheme.nestedTextFieldPadding,
                label: l.pvri,
                required: store.cardiacCatheterizationAvailable ?? false,
                proportions: Fraction(2, 3),
                description: FormField(
                  padding: EdgeInsets.zero,
                  label: l.pvriInput,
                  inputFormatters: [DoubleInputFormatter()],
                  onChanged: (pvri) => store.pvri = tryParseDouble(pvri),
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true, signed: false),
                ),
                checkboxes: Checkboxes(
                  editable: false,
                  selectedValue: store.pvriAbove15WUxMSquared,
                  checkboxInfos: [CheckboxInfo(red, l.pvriHigh, true)],
                ),
              ),
            ),
          ],
        ),
      );

  Widget buildScores(SurveyScores scores) => Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: Observer(
          builder: (context) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (scores.hasInvasiveResults)
                  Row(
                    children: [
                      Expanded(
                        child: ScoreBox(
                          score: scores.lowRiskInvasive!,
                          invasive: true,
                          riskMode: RiskMode.low,
                        ),
                      ),
                      const SizedBox.square(dimension: 12.0),
                      Expanded(
                        child: ScoreBox(
                          score: scores.highRiskInvasive!,
                          invasive: true,
                          riskMode: RiskMode.high,
                        ),
                      ),
                    ],
                  ),
                const SizedBox.square(dimension: 12.0),
                Row(
                  children: [
                    Expanded(
                      child: ScoreBox(
                        score: scores.lowRisk,
                        invasive: false,
                        riskMode: RiskMode.low,
                      ),
                    ),
                    const SizedBox.square(dimension: 12.0),
                    Expanded(
                      child: ScoreBox(
                        score: scores.highRisk,
                        invasive: false,
                        riskMode: RiskMode.high,
                      ),
                    ),
                  ],
                ),
              ],
            );
          },
        ),
      );

  Widget build(BuildContext context) {
    final formTheme = context.formTheme;
    return Scaffold(
      appBar: AppBar(
        title: SelectableText(store.infantScore ? l.phRiskScoreInfant : l.phRiskScore),
      ),
      body: LayoutBuilder(builder: (context, constraints) {
        final constrainedWidth = constraints.constrainWidth(formTheme.maxWidth);
        return SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: (constraints.maxWidth - constrainedWidth) / 2 + 12.0,
            vertical: 12.0,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SelectableText(
                store.infantScore ? l.pleaseEnterDataInfant : l.pleaseEnterData,
                style: title,
              ),

              Markdown(
                data: store.infantScore ? l.noticesInfant : l.notices,
                shrinkWrap: true,
                onTapLink: (_, href, __) => tryLaunch(href!),
                styleSheet: MarkdownStyleSheet(
                  p: formSubtitle,
                  a: formSubtitle.copyWith(decoration: TextDecoration.underline),
                ),
              ),
              SectionTitle(title: l.patientDemographicData),
              const SizedBox(height: 8.0),
              buildPatientData(context, formTheme),
              SectionTitle(title: l.clinicalPresentation),
              buildClinicalPresentation(context, formTheme),
              SectionTitle(title: l.laboratoryResults),
              Observer(
                builder: (context) => Enabled(
                  enabled: !store.infantScore,
                  child: buildLaboratoryResults(context, formTheme),
                ),
              ),
              SectionTitle(title: l.cardiovascularImaging),
              buildMedicalImaging(context, formTheme),
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: SectionTitle(
                        title: l.cardiacCatheterization,
                        padding: EdgeInsets.zero,
                      ),
                    ),
                    Flexible(
                      child: Checkboxes<bool>(
                        editable: true,
                        selectedValue: store.cardiacCatheterizationAvailable,
                        onChanged: (available) =>
                            store.cardiacCatheterizationAvailable = available,
                        checkboxInfos: [
                          CheckboxInfo(black, l.available, true),
                          CheckboxInfo(grey, l.notAvailable, false),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Observer(
                builder: (context) => Observer(
                  builder: (_) => Enabled(
                    enabled: store.cardiacCatheterizationAvailable ?? false,
                    child: buildCardiacCatheterization(context, formTheme),
                  ),
                ),
              ),
              const SizedBox(height: 16.0),
              SelectableText(l.riskScoreDescription, style: description),
              Observer(builder: (context) {
                final scores = store.results.determineScores;
                if (scores == null)
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(height: 16.0),
                      RiskDescription(
                        riskMode: null,
                        text: l.lowerRisk,
                      ),
                      RiskDescription(
                        riskMode: null,
                        text: l.intermediateRisk,
                      ),
                      RiskDescription(
                        riskMode: null,
                        text: l.higherRisk,
                      ),
                    ],
                  );
                else
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      buildScores(scores),
                      SectionTitle(
                        title: l.riskStratificationDescription,
                      ),
                      RiskDescription(
                        riskMode: scores.riskMode == RiskMode.low ? RiskMode.low : null,
                        text: l.lowerRisk,
                      ),
                      RiskDescription(
                        riskMode: scores.riskMode == RiskMode.intermediate
                            ? RiskMode.intermediate
                            : null,
                        text: l.intermediateRisk,
                      ),
                      RiskDescription(
                        riskMode: scores.riskMode == RiskMode.high ? RiskMode.high : null,
                        text: l.higherRisk,
                      ),
                    ],
                  );
              }),
              const SizedBox(height: 16.0),
              const Center(
                child: CreditText(),
              ),
              const SizedBox(height: 8.0),
            ],
          ),
        );
      }),
    );
  }
}

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key? key,
    required this.title,
    this.padding = const EdgeInsets.only(top: 16.0, bottom: 8.0),
  }) : super(key: key);

  final String title;
  final EdgeInsetsGeometry padding;

  Widget build(BuildContext context) => Padding(
        padding: padding,
        child: SelectableText(
          title,
          style: sectionTitle,
        ),
      );
}

class ScoreBox extends StatelessWidget {
  const ScoreBox({
    Key? key,
    required this.riskMode,
    required this.invasive,
    required this.score,
    this.padding = EdgeInsets.zero,
    this.style = subtitle,
  })  : assert(riskMode != RiskMode.intermediate),
        super(key: key);

  final RiskMode riskMode;
  final bool invasive;
  final Fraction score;
  final EdgeInsetsGeometry padding;
  final TextStyle style;

  String getLabel(BuildContext context) {
    final l = context.localization;
    switch (riskMode) {
      case RiskMode.low:
        return invasive ? l.lowerRiskScoreInvasive : l.lowerRiskScore;
      case RiskMode.high:
        return invasive ? l.higherRiskScoreInvasive : l.higherRiskScore;
      case RiskMode.intermediate:
        return "";
    }
  }

  Widget build(BuildContext context) {
    final size = textSize(
      text: "99 ",
      context: context,
      style: style,
    );
    final theme = context.theme;
    return Padding(
      padding: padding,
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(12.0)),
          border: Border.all(
            color: theme.dividerColor,
            width: 1.0,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: SelectableText(
                      getLabel(context),
                      style: sectionTitle.copyWith(
                          color: riskMode == RiskMode.high ? red : green),
                    ),
                  ),
                  if (invasive)
                    Image.asset(
                      "assets/icons/blood_drop.png",
                      width: 24.0,
                      filterQuality: FilterQuality.high,
                      fit: BoxFit.contain,
                    ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SelectableText("${score.numerator}", style: style),
                      SizedBox(
                        width: size.width,
                        child: const Divider(
                          color: Color(0xFF000000),
                          height: 8.0,
                          thickness: 1.0,
                          indent: 0.0,
                          endIndent: 0.0,
                        ),
                      ),
                      SelectableText("${score.denominator}", style: style),
                    ],
                  ),
                  SelectableText("  =  ", style: style),
                  SelectableText(
                    score.toDouble().toStringAsFixed(2),
                    style: style.copyWith(
                      fontSize: style.fontSize.map((s) => s * 1.25),
                    ),
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class RiskDescription extends StatelessWidget {
  const RiskDescription({
    Key? key,
    required this.riskMode,
    required this.text,
    this.padding = const EdgeInsetsDirectional.fromSTEB(0.0, 10.0, 0.0, 10.0),
  }) : super(key: key);

  final EdgeInsetsGeometry padding;
  final RiskMode? riskMode;
  final String text;

  Color get color {
    switch (riskMode) {
      case RiskMode.low:
        return green;
      case RiskMode.intermediate:
        return yellow;
      case RiskMode.high:
        return red;
      default:
        return grey;
    }
  }

  Widget build(BuildContext context) => SizedBox(
        width: double.infinity,
        child: Padding(
          padding: padding,
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: const Color(0xFFFFFFFF),
              border: Border.all(color: color),
              borderRadius: const BorderRadius.all(Radius.circular(12.0)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 12.0,
                  color: color.withAlpha((255 * 0.25).toInt()),
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: SelectableText(text, style: title.copyWith(color: color)),
            ),
          ),
        ),
      );
}

class CreditText extends StatelessWidget {
  const CreditText({Key? key}) : super(key: key);

  Widget build(BuildContext context) => MouseRegion(
        cursor: SystemMouseCursors.click,
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => tryLaunch(context.localization.nexamUrl),
          child: RichText(
            text: const TextSpan(
              style: TextStyle(
                fontSize: 20.0,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
                height: 30.0 / 20.0,
                letterSpacing: 0.15,
                fontFamily: "Roboto",
                color: Color(0xFF000000),
              ),
              children: [
                TextSpan(text: "Programmed by "),
                TextSpan(
                  style: TextStyle(fontFamily: "Ubuntu"),
                  children: [
                    TextSpan(text: "ne"),
                    TextSpan(text: "x", style: TextStyle(color: Color(0xFFFF6525))),
                    TextSpan(text: "am"),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
}
