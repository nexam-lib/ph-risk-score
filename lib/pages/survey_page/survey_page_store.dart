import 'dart:math' as math;

import 'package:mobx/mobx.dart';
import 'package:phindex/models/classes.dart';
import 'package:phindex/models/enums.dart';
import 'package:phindex/models/fraction.dart';
import 'package:phindex/models/survey_result_store.dart';
import 'package:phindex/util/formula.dart';

import '../../models/enums.dart';
import '../../models/fraction.dart';
import '../../util/util.dart';

part 'survey_page_store.g.dart';

class SurveyStore = SurveyStoreBase with _$SurveyStore;

abstract class SurveyStoreBase with Store {
  bool durationLongerThan1Year(DateTime? a, DateTime? b) {
    if (a == null || b == null)
      return false;
    else
      return a.difference(b).abs() > const Duration(days: 365);
  }

  final SurveyResultStore results;
  late final List<ReactionDisposer> reactions;

  SurveyStoreBase(this.infantScore) : results = SurveyResultStore(infantScore) {
    reactions = [
      reaction((_) => clinicalEvidenceOfRvFailure,
          results.evaluateClinicalEvidenceOfRVFailure),
      reaction((_) => progressionOfSymptoms, results.evaluateProgressionOfSymptoms),
      reaction((_) => syncopeInTheLast6Months, results.evaluateSyncopeInLast6Months),
      reaction((_) => growth, results.evaluateGrowth),
      reaction((_) => whoFunctionalClass, results.evaluateWhoFunctionalClass),
      reaction((_) => [dateOfClinicalPresentation, dateOfBloodExamination], (_) {
        final invalid =
            durationLongerThan1Year(dateOfClinicalPresentation, dateOfBloodExamination);
        if (invalid && !moreThan1YearBetweenClinicalPresentationAndBloodExamination) {
          serumNTproBNP = null;
          rapidlyRisingNTProBNPLevel = false;
        }
        moreThan1YearBetweenClinicalPresentationAndBloodExamination = invalid;
        results.dateOfBloodExaminationValid =
            !moreThan1YearBetweenClinicalPresentationAndBloodExamination;
      }),
      reaction<SerumNTproBNP?>((_) => serumNTproBNPLevel,
          (level) => results.evaluateSerumNTproBNPLevel(age, level)),
      reaction((_) => [dateOfClinicalPresentation, dateOfEchoCardiography], (_) {
        moreThan1YearBetweenClinicalPresentationAndEchocardiography =
            durationLongerThan1Year(dateOfClinicalPresentation, dateOfEchoCardiography);
        results.dateOfEchocardiographyValid =
            !moreThan1YearBetweenClinicalPresentationAndEchocardiography;
      }),
      reaction((_) => rarvEnlargement, results.evaluateRARVEnlargement),
      reaction((_) => rvSystolivDysfunction, results.evaluateRVSystolicDysfunction),
      reaction((_) => rvlvEndsystolicRatioLevel, results.evaluateRVLVEndsystolicRatio),
      reaction((_) => tapseLevel, results.evaluateTAPSE),
      reaction<SDRatioLevel?>((_) => sdRatioLevel,
          (level) => results.evaluateSDRatio(sdRatioAvailable, level)),
      reaction<bool>((_) => sdRatioAvailable,
          (available) => results.evaluateSDRatio(available, sdRatioLevel)),
      reaction<PAATLevel?>(
          (_) => paatLevel, (level) => results.evaluatePAAT(paatAvailable, age, level)),
      reaction<bool>((_) => paatAvailable,
          (available) => results.evaluatePAAT(available, age, paatLevel)),
      reaction((_) => pericardialEffusion, results.evaluatePericardialEffusion),
      reaction(
          (_) => [
                dateOfClinicalPresentation,
                dateOfEchoCardiography,
                dateOfLastCathStudy
              ], (_) {
        moreThan1YearBetweenCathStudyAndOtherExaminations =
            durationLongerThan1Year(dateOfLastCathStudy, dateOfClinicalPresentation) ||
                durationLongerThan1Year(dateOfLastCathStudy, dateOfEchoCardiography);
        results.dateOfLastCathStudyValid =
            !moreThan1YearBetweenCathStudyAndOtherExaminations;
      }),
      reaction((_) => cardiacIndexLevel, results.evaluateCardiacIndex),
      reaction((_) => mRAPLevel, results.evaluateMRAP),
      reaction((_) => mPAPmSAPLevel, results.evaluateMPAPmSAP),
      reaction(
          (_) => acuteVasoreactivityTesting, results.evaluateAcuteVasoreactivityTesting),
      reaction<bool?>(
          (_) => pvriAbove15WUxMSquared, (above) => results.evaluatePVRi(pvri, above)),
    ];
  }

  void dispose() {
    for (final reaction in reactions) {
      reaction();
    }
  }

  final bool infantScore;

  /**
   * Patient Data
   */
  @observable
  String fullName = "";
  @observable
  DateTime? dateOfBirth;
  @computed
  double? get age {
    final dOb = dateOfBirth;
    final dOCp = dateOfClinicalPresentation;
    if (dOb == null || dOCp == null) return null;

    final ageAtTest = dOCp.difference(dOb).inDays;
    final ageDec = roundDouble(ageAtTest / 365, 1);
    if (ageDec == null) return null;
    return (2 * ageDec).floorToDouble() / 2;
  }

  @computed
  bool get moreThan1YearOld => age != null && age! > 1;
  @observable
  String patientId = "";
  @observable
  double? weight;
  @observable
  double? height;
  @observable
  Sex? sex;

  /// in m²
  @computed
  double? get bodySurfaceArea {
    final weight = this.weight;
    final height = this.height;

    return weight != null && height != null
        ? roundDouble(math.pow((height * weight) / 3600, 0.5).toDouble(), 2)
        : null;
  }

  /// in kg/m²
  @computed
  double? get bodyMassIndex {
    final weight = this.weight;
    final height = this.height;

    return height != null && weight != null
        ? roundDouble(weight / math.pow(height / 100, 2), 1)
        : null;
  }

  @observable
  Race? race;
  @observable
  String freeTextRace = "";
  @observable
  String? country;
  @observable
  Continent? continent;
  @observable
  String phCenter = "";
  @observable
  PhClassification? phClassification;
  @observable
  PhClassification? otherPhClassification;
  @observable
  String otherPhClassificationDiagnosis = "";

  /**
   * Clinical Presentation
   */
  @observable
  DateTime? dateOfClinicalPresentation;
  @observable
  bool? clinicalEvidenceOfRvFailure;
  @observable
  bool? progressionOfSymptoms;
  @observable
  bool? syncopeInTheLast6Months;
  @computed
  AgePercentile? get bmiPercentile {
    final age = this.age;
    final sex = this.sex;
    if (age == null || sex == null) return null;

    return getBmiPercentile(sex, age);
  }

  @computed
  AgePercentile? get heightPercentile {
    final age = this.age;
    final sex = this.sex;
    if (age == null || sex == null) return null;

    return getHeightPercentile(sex, age);
  }

  @computed
  Growth? get growth =>
      calcGrowth(bodyMassIndex, bmiPercentile, height, heightPercentile);
  @observable
  WHOFunctionalClass? whoFunctionalClass;

  /**
   * Laboratory Results
   */
  @observable
  DateTime? dateOfBloodExamination;
  @observable
  bool moreThan1YearBetweenClinicalPresentationAndBloodExamination = false;
  @computed
  bool get atLeast1YearOldAtDateOfBloodExamination {
    final dob = dateOfBirth;
    final dobe = dateOfBloodExamination;
    return dob != null &&
        dobe != null &&
        dobe.difference(dob) < const Duration(days: 365);
  }

  @observable
  double? serumNTproBNP;
  @computed
  SerumNTproBNP? get serumNTproBNPLevel {
    final serumNTproBNP = this.serumNTproBNP;
    if (serumNTproBNP == null) return null;

    if (rapidlyRisingNTProBNPLevel || serumNTproBNP > 1200.0)
      return SerumNTproBNP.greatlyElevated;
    else if (serumNTproBNP < 250.0)
      return SerumNTproBNP.minimallyElevated;
    else
      return SerumNTproBNP.moderatedlyElevated;
  }

  @observable
  bool rapidlyRisingNTProBNPLevel = false;

  /**
   * Medical Imaging
   */
  @observable
  DateTime? dateOfEchoCardiography;
  @observable
  bool moreThan1YearBetweenClinicalPresentationAndEchocardiography = false;
  @observable
  RARVEnlargement? rarvEnlargement;
  @observable
  RVSystolicDysfunction? rvSystolivDysfunction;
  @observable
  double? rvlvEndsystolicRatio;
  @computed
  RVLVEndsystolicRatio? get rvlvEndsystolicRatioLevel {
    final ratio = rvlvEndsystolicRatio;
    if (ratio == null) return null;

    if (ratio < 1.0)
      return RVLVEndsystolicRatio.below1;
    else if (ratio > 1.5)
      return RVLVEndsystolicRatio.severe;
    else
      return RVLVEndsystolicRatio.between1And1Point5;
  }

  /// Stands for Tricuspid Annular Plane Systolic Excursion
  /// in cm, no more than 2 decimal digits
  @observable
  double? tapse;
  @computed
  double? get zScore {
    final dateOfBirth = this.dateOfBirth;
    final dateOfClPres = dateOfClinicalPresentation;
    final tapse = this.tapse;
    if (dateOfBirth == null || tapse == null || dateOfClPres == null) return null;

    return calcZScore(dateOfClPres, dateOfBirth, tapse);
  }

  @computed
  TAPSELevel? get tapseLevel {
    final tapse = this.tapse;
    final zScore = this.zScore;
    if (tapse == null || zScore == null) return null;

    if (zScore < -3)
      return TAPSELevel.belowMinus3;
    else if (zScore > -2)
      return TAPSELevel.normal;
    else
      return TAPSELevel.moderateLower;
  }

  @observable
  double? sdRatio;
  @observable
  bool sdRatioAvailable = true;
  @computed
  SDRatioLevel? get sdRatioLevel {
    final sdRatio = this.sdRatio;
    if (sdRatio == null) return null;
    if (sdRatio < 1)
      return SDRatioLevel.below1;
    else if (sdRatio >= 1 && sdRatio <= 1.4)
      return SDRatioLevel.between1And1point4;
    else
      return SDRatioLevel.above1Point4;
  }

  /// Only include if 1 year old or older
  /// in ms
  /// Pulmonary Artery Acceleration Time
  @observable
  double? paat;
  @observable
  bool paatAvailable = true;
  @computed
  double? get paatzScore {
    final age = this.age;
    final sex = this.sex;
    final paat = this.paat;
    if (dateOfBirth == null ||
        dateOfClinicalPresentation == null ||
        sex == null ||
        age == null ||
        paat == null) return null;

    return calcPaatZScore(paat, sex, age);
  }

  @computed
  PAATLevel? get paatLevel {
    final paat = this.paat;
    if (paat == null) return null;

    if (paat < 70)
      return PAATLevel.below70;
    else if (paat >= 70 && paat <= 100)
      return PAATLevel.between70And100;
    else if (paat > 100) return PAATLevel.over100;
  }

  @observable
  PericardialEffusion? pericardialEffusion;

  /**
   * Cardiac Catherization
   */
  @observable
  bool? _cardiacCatheterizationAvailable;
  bool? get cardiacCatheterizationAvailable => _cardiacCatheterizationAvailable;
  set cardiacCatheterizationAvailable(bool? available) {
    _cardiacCatheterizationAvailable = available;
    results.cardiacCatheterizationAvailable = available;
  }

  @observable
  DateTime? dateOfLastCathStudy;
  @computed
  bool get lastCathStudyValid {
    var date = dateOfLastCathStudy;
    if (date == null) return false;
    return date.isBefore(DateTime.now());
  }

  @observable
  bool moreThan1YearBetweenCathStudyAndOtherExaminations = false;

  /// in l/min/m²
  @observable
  double? cardiacIndex;
  @computed
  CardiacIndexLevel? get cardiacIndexLevel {
    final cardiacIndex = this.cardiacIndex;
    if (cardiacIndex == null) return null;
    if (cardiacIndex > 3)
      return CardiacIndexLevel.over3;
    else if (cardiacIndex <= 3 && cardiacIndex >= 2.5)
      return CardiacIndexLevel.between3And2Point5;
    else if (cardiacIndex < 2.5) return CardiacIndexLevel.below2Point5;
  }

  @observable
  double? mRAP;
  @computed
  MRAPLevel? get mRAPLevel {
    final mRAP = this.mRAP;
    if (mRAP == null) return null;

    if (mRAP < 10)
      return MRAPLevel.below10;
    else if (mRAP >= 10 && mRAP <= 15)
      return MRAPLevel.between10And15;
    else if (mRAP > 15) return MRAPLevel.over15;
  }

  @observable
  int? mPAP;
  @observable
  int? mSAP;
  @computed
  double? get mPAPDividedByMSAP => mPAP != null && mSAP != null ? (mPAP! / mSAP!) : null;
  @computed
  MPAPmSAPLevel? get mPAPmSAPLevel {
    final mPAPDividedByMSAP = this.mPAPDividedByMSAP;
    if (mPAPDividedByMSAP == null) return null;
    if (mPAPDividedByMSAP < 0.5)
      return MPAPmSAPLevel.below0Point5;
    else if (mPAPDividedByMSAP >= 0.5 && mPAPDividedByMSAP <= 0.75)
      return MPAPmSAPLevel.between0Point5And0Point75;
    else if (mPAPDividedByMSAP > 0.75) return MPAPmSAPLevel.above0Point75;
  }

  @observable
  AcuteVasoreactivityTesting? acuteVasoreactivityTesting;
  @observable
  double? pvri;
  @computed
  bool? get pvriAbove15WUxMSquared {
    final pvri = this.pvri;
    if (pvri == null) return null;
    return pvri > 15;
  }

  @observable
  Fraction? lowRiskScore;
  @observable
  Fraction? highRiskScore;
  @observable
  Fraction? lowRiskInvasiveScore;
  @observable
  Fraction? highRiskInvasiveScore;
  @observable
  Duration? duration;

  @observable
  RiskMode? riskMode;

  @computed
  bool get valid =>
      personalDataValid &&
      clinicalPresentationValid &&
      laboratoryResultsValid &&
      medicalImagingValid &&
      cardiacCatheterizationValid;

  @computed
  bool get personalDataValid =>
      dateOfBirth != null && weight != null && height != null && sex != null;

  @computed
  bool get clinicalPresentationValid =>
      dateOfClinicalPresentation != null &&
      clinicalEvidenceOfRvFailure != null &&
      progressionOfSymptoms != null &&
      syncopeInTheLast6Months != null &&
      whoFunctionalClass != null;

  @computed
  bool get laboratoryResultsValid =>
      dateOfBloodExamination != null && serumNTproBNP != null;

  @computed
  bool get medicalImagingValid =>
      dateOfEchoCardiography != null &&
      rarvEnlargement != null &&
      rvSystolivDysfunction != null &&
      rvlvEndsystolicRatio != null &&
      tapse != null &&
      (!sdRatioAvailable || sdRatio != null) &&
      (!paatAvailable || paat != null) &&
      pericardialEffusion != null;

  @computed
  bool get cardiacCatheterizationValid {
    final cardiacCatheterizationAvailable = this.cardiacCatheterizationAvailable;
    if (cardiacCatheterizationAvailable == null)
      return false;
    else if (!cardiacCatheterizationAvailable)
      return true;
    else
      return dateOfLastCathStudy != null &&
          lastCathStudyValid &&
          cardiacIndex != null &&
          mRAP != null &&
          mPAP != null &&
          mSAP != null &&
          acuteVasoreactivityTesting != null &&
          pvri != null;
  }

  void evaluateBlocks() {
    assert(valid);
    assert(age != null);
    final t = Stopwatch()..start();
    final riskState = RiskState();

    try {
      {
        // clinicalEvidenceOfRvFailure

        riskState.addRiskFor("clinicalEvidenceOfRvFailure",
            isHighRisk: clinicalEvidenceOfRvFailure);
        log("Clincal Evidende:\n" + riskState.toString());

        riskState.addRiskFor("progressionOfSymptoms", isHighRisk: progressionOfSymptoms);
        log("progressionOfSymptoms:\n" + riskState.toString());

        riskState.addRiskFor("syncopeInTheLast6Months",
            isHighRisk: syncopeInTheLast6Months);
        log("syncopeInTheLast6Months:\n" + riskState.toString());
      }

      {
        final grwth = growth;
        if (grwth == null) throw ArgumentError("growth not set");
        switch (grwth) {
          case Growth.normal:
            riskState.addRiskFor("growth", isHighRisk: false);
            break;
          case Growth.slightlyRestricted:
            riskState.addRiskFor("growth");
            break;
          case Growth.failureToThrive:
            riskState.addRiskFor("growth", isHighRisk: true);
            break;
        }
        log("grwth:\n" + riskState.toString());
      }
      {
        final who = whoFunctionalClass;
        if (who == null) throw ArgumentError("whoFunctionalClass not set");
        switch (who) {
          case WHOFunctionalClass.I:
          case WHOFunctionalClass.II:
            riskState.addRiskFor("whoFunctionalClass", isHighRisk: false, value: 2);
            break;
          case WHOFunctionalClass.III:
          case WHOFunctionalClass.IV:
            riskState.addRiskFor("whoFunctionalClass", isHighRisk: true, value: 2);
            break;
        }
        log("whoFunctionalClass:\n" + riskState.toString());
      }

      if (age! > 1) {
        // no calculation for moderatedlyElevated
        final serumLvl = serumNTproBNPLevel;
        if (serumLvl == null) throw ArgumentError("serumNTproBNPLevel not set");

        switch (serumLvl) {
          case SerumNTproBNP.minimallyElevated:
            riskState.addRiskFor("serumNTproBNPLevel", isHighRisk: false, value: 2);
            break;
          case SerumNTproBNP.moderatedlyElevated:
            riskState.addRiskFor("serumNTproBNPLevel", value: 2);
            break;
          case SerumNTproBNP.greatlyElevated:
            riskState.addRiskFor("serumNTproBNPLevel", isHighRisk: true, value: 2);
            break;
        }
        log("serumNTproBNPLevel:\n" + riskState.toString());
      }

      {
        final rarv = rarvEnlargement;
        if (rarv == null) throw ArgumentError("rarvEnlargement not set");
        switch (rarv) {
          case RARVEnlargement.minimal:
            riskState.addRiskFor("rarvEnlargement", isHighRisk: false, value: 1);
            break;
          case RARVEnlargement.moderate:
            riskState.addRiskFor("rarvEnlargement");
            break;
          case RARVEnlargement.severe:
            riskState.addRiskFor("rarvEnlargement", isHighRisk: true, value: 1);
            break;
        }
        log("rarvEnlargement:\n" + riskState.toString());
      }

      {
        final rvs = rvSystolivDysfunction;
        if (rvs == null) throw ArgumentError("rvSystolivDysfunction not set");
        switch (rvs) {
          case RVSystolicDysfunction.noDysfunction:
            riskState.addRiskFor("rvSystolivDysfunction", isHighRisk: false, value: 1);
            break;
          case RVSystolicDysfunction.moderateDysfunction:
            riskState.addRiskFor("rvSystolivDysfunction");
            break;
          case RVSystolicDysfunction.dysfunction:
            riskState.addRiskFor("rvSystolivDysfunction", isHighRisk: true, value: 1);
            break;
        }
        log("rvSystolivDysfunction:\n" + riskState.toString());
      }

      {
        final rvlvE = rvlvEndsystolicRatioLevel;
        if (rvlvE == null) throw ArgumentError("rvlvEndsystolicRatioLevel not set");
        switch (rvlvE) {
          case RVLVEndsystolicRatio.below1:
            riskState.addRiskFor("rvlvEndsystolicRatioLevel",
                isHighRisk: false, value: 1);
            break;
          case RVLVEndsystolicRatio.between1And1Point5:
            riskState.addRiskFor("rvlvEndsystolicRatioLevel");
            break;
          case RVLVEndsystolicRatio.severe:
            riskState.addRiskFor("rvlvEndsystolicRatioLevel", isHighRisk: true, value: 1);
            break;
        }
        log("rvlvEndsystolicRatioLevel:\n" + riskState.toString());
      }

      {
        final tapseLvl = tapseLevel;
        if (tapseLvl == null) throw ArgumentError("tapseLevel not set");
        switch (tapseLvl) {
          case TAPSELevel.normal:
            riskState.addRiskFor("tapseLevel", isHighRisk: false, value: 1);
            break;
          case TAPSELevel.moderateLower:
            riskState.addRiskFor("tapseLevel");
            break;
          case TAPSELevel.belowMinus3:
            riskState.addRiskFor("tapseLevel", isHighRisk: true, value: 1);
            break;
        }
        log("tapseLevel:\n" + riskState.toString());
      }

      {
        final sdRatiolvl = sdRatioLevel;
        // weil optional nur setzen wenn da
        if (sdRatiolvl != null) {
          switch (sdRatiolvl) {
            case SDRatioLevel.below1:
              riskState.addRiskFor("sdRatioLevel", isHighRisk: false, value: 1);
              break;
            case SDRatioLevel.between1And1point4:
              riskState.addRiskFor("sdRatioLevel");
              break;
            case SDRatioLevel.above1Point4:
              riskState.addRiskFor("sdRatioLevel", isHighRisk: true, value: 1);
              break;
          }
        }
        log("sdRationlvl:\n" + riskState.toString());
      }

      if (age! > 1) {
        final paatLvl = paatLevel;
        // weil optional nur setzen wenn da
        if (paatLvl != null) {
          switch (paatLvl) {
            case PAATLevel.over100:
              riskState.addRiskFor("paatLevel", isHighRisk: false, value: 1);
              break;
            case PAATLevel.between70And100:
              riskState.addRiskFor("paatLevel");
              break;
            case PAATLevel.below70:
              riskState.addRiskFor("paatLevel", isHighRisk: true, value: 1);
              break;
          }
        }
        log("paatLevel:\n" + riskState.toString());
      }
      {
        final pericardialEf = pericardialEffusion;
        if (pericardialEf != null) {
          switch (pericardialEf) {
            case PericardialEffusion.pericardialEffusion:
              riskState.highRisk.numerator += 1;
              riskState.highRisk.denominator += 1;
              break;
            case PericardialEffusion.noPericardialEffusion:
              riskState.highRisk.denominator += 1;
              break;
          }
        }
        log("pericardialEf:\n" + riskState.toString());
      }

      log("Before cath:\n" + riskState.toString());

      if (cardiacCatheterizationAvailable ?? false) {
        riskState.cathLowRisk = riskState.lowRisk.copyWith();
        riskState.cathHighRisk = riskState.highRisk.copyWith();

        log("======================================================");
        // here starts invasive
        {
          final cardiacIndLvl = cardiacIndexLevel;
          if (cardiacIndLvl == null) {
            throw ArgumentError("cardiacIndex not set");
          }
          switch (cardiacIndLvl) {
            case CardiacIndexLevel.over3:
              riskState.addCathRiskFor("cardiacIndex", isHighRisk: false, value: 2);
              break;
            case CardiacIndexLevel.between3And2Point5:
              riskState.addCathRiskFor("cardiacIndex", value: 2);
              break;
            case CardiacIndexLevel.below2Point5:
              riskState.addCathRiskFor("cardiacIndex", isHighRisk: true, value: 2);
              break;
          }
        }
        log("caridacIndexLevel:\n" + riskState.toString());

        {
          final mRAPLvl = mRAPLevel;
          if (mRAPLvl == null) {
            throw ArgumentError("mRAPLevel not set");
          }
          switch (mRAPLvl) {
            case MRAPLevel.below10:
              riskState.addCathRiskFor("mRAPLevel", isHighRisk: false, value: 2);
              break;
            case MRAPLevel.between10And15:
              riskState.addCathRiskFor("mRAPLevel", value: 2);
              break;
            case MRAPLevel.over15:
              riskState.addCathRiskFor("mRAPLevel", isHighRisk: true, value: 2);
              break;
          }
        }
        log("mRAPLevel:\n" + riskState.toString());

        {
          final mPAPmSAPLvl = mPAPmSAPLevel;
          if (mPAPmSAPLvl == null) {
            throw ArgumentError("mPAPmSAPLevel not set");
          }
          switch (mPAPmSAPLvl) {
            case MPAPmSAPLevel.below0Point5:
              riskState.addCathRiskFor("mPAPmSAPLevel", isHighRisk: false, value: 1);
              break;
            case MPAPmSAPLevel.between0Point5And0Point75:
              riskState.addCathRiskFor("mPAPmSAPLevel", value: 1);
              break;
            case MPAPmSAPLevel.above0Point75:
              riskState.addCathRiskFor("mPAPmSAPLevel", isHighRisk: true, value: 1);
              break;
          }
        }
        log("mPAPmSAPLevel:\n" + riskState.toString());

        {
          final acuteTesting = acuteVasoreactivityTesting;
          if (acuteTesting == null) {
            throw ArgumentError("acuteVasoreactivityTesting not set");
          }
          switch (acuteTesting) {
            case AcuteVasoreactivityTesting.positive:
              riskState.cathLowRisk.numerator += 1;
              riskState.cathLowRisk.denominator += 1;
              break;
            case AcuteVasoreactivityTesting.negative:
              riskState.cathLowRisk.denominator += 1;
              break;
            case AcuteVasoreactivityTesting.notAvailable:
              // do nothing
              break;
          }
        }
        log("acuteVasoreactivityTesting:\n" + riskState.toString());
        {
          if (pvri != null) {
            if (pvriAbove15WUxMSquared != null && pvriAbove15WUxMSquared!) {
              riskState.cathHighRisk.numerator += 1;
            }
            riskState.cathHighRisk.denominator += 1;
          }
        }
        log("pvriAbove15WUxMSquared:\n" + riskState.toString());

        lowRiskInvasiveScore = riskState.cathLowRisk;
        highRiskInvasiveScore = riskState.cathHighRisk;
      }

      log("Final Results:\n" + riskState.toString());
      lowRiskScore = riskState.lowRisk;
      highRiskScore = riskState.highRisk;
      duration = t.elapsed;
      t.stop();
    } catch (e) {
      log("Failed to evaluate blocks $e");
      lowRiskScore = null;
      highRiskScore = null;
      lowRiskInvasiveScore = null;
      highRiskInvasiveScore = null;
    }
  }

  void stratifyRisk() {
    if (!valid || lowRiskScore == null) return riskMode = null;

    int starredLowRisk = 0;
    int nonStarredLowRisk = 0;
    int starredHighRisk = 0;
    int nonStarredHighRisk = 0;

    if (clinicalEvidenceOfRvFailure!)
      ++nonStarredHighRisk;
    else
      ++nonStarredLowRisk;

    if (progressionOfSymptoms!) {
      ++nonStarredHighRisk;
    } else {
      ++nonStarredLowRisk;
    }

    if (syncopeInTheLast6Months!)
      ++nonStarredHighRisk;
    else
      ++nonStarredLowRisk;

    switch (growth!) {
      case Growth.normal:
        ++nonStarredLowRisk;
        break;
      case Growth.slightlyRestricted:
        break;
      case Growth.failureToThrive:
        ++nonStarredHighRisk;
        break;
    }

    switch (whoFunctionalClass!) {
      case WHOFunctionalClass.I:
      case WHOFunctionalClass.II:
        ++starredLowRisk;
        break;
      case WHOFunctionalClass.III:
      case WHOFunctionalClass.IV:
        ++starredHighRisk;
    }

    switch (serumNTproBNPLevel!) {
      case SerumNTproBNP.minimallyElevated:
        ++starredLowRisk;
        break;
      case SerumNTproBNP.moderatedlyElevated:
        break;
      case SerumNTproBNP.greatlyElevated:
        ++starredHighRisk;
        break;
    }

    switch (rarvEnlargement!) {
      case RARVEnlargement.minimal:
        ++nonStarredLowRisk;
        break;
      case RARVEnlargement.moderate:
        break;
      case RARVEnlargement.severe:
        ++nonStarredHighRisk;
        break;
    }

    switch (rvSystolivDysfunction!) {
      case RVSystolicDysfunction.noDysfunction:
        ++nonStarredLowRisk;
        break;
      case RVSystolicDysfunction.moderateDysfunction:
        break;
      case RVSystolicDysfunction.dysfunction:
        ++nonStarredHighRisk;
        break;
    }

    switch (rvlvEndsystolicRatioLevel!) {
      case RVLVEndsystolicRatio.below1:
        ++nonStarredLowRisk;
        break;
      case RVLVEndsystolicRatio.between1And1Point5:
        break;
      case RVLVEndsystolicRatio.severe:
        ++nonStarredHighRisk;
        break;
    }

    switch (tapseLevel!) {
      case TAPSELevel.normal:
        ++nonStarredLowRisk;
        break;
      case TAPSELevel.moderateLower:
        break;
      case TAPSELevel.belowMinus3:
        ++nonStarredHighRisk;
        break;
    }

    if (sdRatioAvailable) {
      switch (sdRatioLevel!) {
        case SDRatioLevel.below1:
          ++nonStarredLowRisk;
          break;
        case SDRatioLevel.between1And1point4:
          break;
        case SDRatioLevel.above1Point4:
          ++nonStarredHighRisk;
          break;
      }
    }

    if (paatAvailable) {
      switch (paatLevel!) {
        case PAATLevel.over100:
          ++nonStarredLowRisk;
          break;
        case PAATLevel.between70And100:
          break;
        case PAATLevel.below70:
          ++nonStarredHighRisk;
          break;
      }
    }

    if (pericardialEffusion == PericardialEffusion.pericardialEffusion)
      ++nonStarredHighRisk;

    if (cardiacCatheterizationAvailable ?? false) {
      switch (cardiacIndexLevel!) {
        case CardiacIndexLevel.over3:
          ++starredLowRisk;
          break;
        case CardiacIndexLevel.between3And2Point5:
          break;
        case CardiacIndexLevel.below2Point5:
          ++starredHighRisk;
          break;
      }

      switch (mRAPLevel!) {
        case MRAPLevel.below10:
          ++starredLowRisk;
          break;
        case MRAPLevel.between10And15:
          break;
        case MRAPLevel.over15:
          ++starredHighRisk;
          break;
      }

      switch (mPAPmSAPLevel!) {
        case MPAPmSAPLevel.below0Point5:
          ++nonStarredLowRisk;
          break;
        case MPAPmSAPLevel.between0Point5And0Point75:
          break;
        case MPAPmSAPLevel.above0Point75:
          ++nonStarredHighRisk;
          break;
      }

      switch (acuteVasoreactivityTesting!) {
        case AcuteVasoreactivityTesting.positive:
          ++nonStarredLowRisk;
          break;
        case AcuteVasoreactivityTesting.negative:
          break;
        case AcuteVasoreactivityTesting.notAvailable:
          break;
      }

      if (pvriAbove15WUxMSquared != null && pvriAbove15WUxMSquared!) ++nonStarredHighRisk;
    }

    if (starredLowRisk >= 3 && starredHighRisk == 0 && nonStarredHighRisk == 0)
      riskMode = RiskMode.low;
    else if (nonStarredLowRisk >= 5 && starredHighRisk == 0 && nonStarredLowRisk == 0)
      riskMode = RiskMode.low;
    else if (starredHighRisk >= 2 && cardiacIndexLevel == CardiacIndexLevel.below2Point5)
      riskMode = RiskMode.high;
    else if (serumNTproBNPLevel == SerumNTproBNP.greatlyElevated &&
        nonStarredHighRisk >= 5)
      riskMode = RiskMode.high;
    else
      riskMode = RiskMode.intermediate;
  }
}
