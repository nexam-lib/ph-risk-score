import 'dart:ui';

import 'package:flutter/material.dart';

const formFieldPadding = 16.0;

const green = Color(0xFF00A400);
const yellow = Color(0xFFDD8E2F);
const red = Color(0xFFDF2020);
const grey = Color(0xFF979797);
const almostBlack = Color(0xFF1C1B1F);
const black = Color(0xFF000000);

class FormTheme {
  const FormTheme({
    required this.maxWidth,
    required this.containerPadding,
    required this.containerMargin,
    required this.textFieldPadding,
    required this.fieldTitleGap,
    required this.nestedTextFieldPadding,
    required this.customChildrenGap,
    required this.fieldPadding,
  });

  /// Maximum width of the form. Use this to ensure that the app doesn't look
  /// stupidly wide.
  final double maxWidth;

  /// Padding outside of [FormContainer] widgets
  final EdgeInsetsDirectional containerPadding;

  /// Margin inside decoration of [FormContainer] widgets
  final EdgeInsetsDirectional containerMargin;

  /// Padding outside [TextField] widgets
  final EdgeInsetsDirectional textFieldPadding;

  /// Gap between a fields title and its content
  final double fieldTitleGap;

  /// Padding outside [TextField] widgets that are part of more complex fields
  final EdgeInsetsDirectional nestedTextFieldPadding;

  /// Gap between children of a [CustomTripleCheckbox] widget
  final double customChildrenGap;

  /// Padding outside form field widgets
  final EdgeInsetsDirectional fieldPadding;
}

/// Used in headlines above sections of the form
const sectionTitle = TextStyle(
  color: Color(0xFF000000),
  fontWeight: FontWeight.w500,
  fontStyle: FontStyle.normal,
  fontSize: 24.0,
  height: 30.0 / 24.0,
  letterSpacing: 0.15,
);

/// Used in the titles of form fields
const subtitle = TextStyle(
  color: almostBlack,
  fontWeight: FontWeight.w500,
  fontStyle: FontStyle.normal,
  fontSize: 20.0,
  height: 24.0 / 20.0,
  letterSpacing: 0.1,
);

/// Used in the titles of form fields
const title = TextStyle(
  color: almostBlack,
  fontWeight: FontWeight.w500,
  fontStyle: FontStyle.normal,
  fontSize: 16.0,
  height: 24.0 / 16.0,
  letterSpacing: 0.1,
);

/// Used in descriptions, duh
const description = TextStyle(
  color: almostBlack,
  fontWeight: FontWeight.w400,
  fontStyle: FontStyle.normal,
  fontSize: 14.0,
  height: 20.0 / 14.0,
  letterSpacing: 0.25,
);

/// Used in checkbox labels.
/// In some places, the mockups use 12 as fontSize and 0.5% as letterSpacing.
const checkbox = TextStyle(
  fontWeight: FontWeight.w500,
  fontStyle: FontStyle.normal,
  fontSize: 14.0,
  height: 14.0 / 16.0,
  letterSpacing: 0.5,
);

const formSubtitle = TextStyle(
  color: Color(0xFF000000),
  fontWeight: FontWeight.w500,
  fontStyle: FontStyle.normal,
  fontSize: 12.0,
  letterSpacing: 0.5,
);
