import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart' as intl;
import 'dart:math';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:phindex/util/theme.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

typedef Localizer<T> = String Function(T);

const shouldLog = kDebugMode;

void log(Object? s) {
  // ignore: avoid_print
  if (shouldLog) print(s);
}

Future<bool> tryLaunch(String url) async {
  try {
    return launch(url);
  } catch (e) {
    log("Could not launch URL $url");
    return Future.value(false);
  }
}

String localizeDate(BuildContext context, DateTime date) =>
    intl.DateFormat.yMMMMd(Localizations.localeOf(context).toLanguageTag()).format(date);

class IntInputFormatter extends TextInputFormatter {
  IntInputFormatter([this.validator = _alwaysTrueValidator]);

  final bool Function(int) validator;

  static bool _alwaysTrueValidator(int _) => true;

  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.isEmpty) return newValue;

    final intValue = int.tryParse(newValue.text);
    return intValue != null && validator(intValue) ? newValue : oldValue;
  }
}

class DoubleInputFormatter extends TextInputFormatter {
  DoubleInputFormatter({
    this.acceptCommas = true,
    this.validator = _alwaysTrueValidator,
  });

  final bool Function(double) validator;
  final bool acceptCommas;

  static bool _alwaysTrueValidator(double _) => true;

  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.isEmpty || newValue.text == "-")
      return newValue;

    final doubleValue = acceptCommas
      ? tryParseDouble(newValue.text)
      : double.tryParse(newValue.text);
    return doubleValue != null && validator(doubleValue) ? newValue : oldValue;
  }
}

double? tryParseDouble(String s) => double.tryParse(s.replaceAll(",", "."));

extension NullableExtension<T> on T? {
  U? map<U>(U Function(T) f) {
    final self = this;
    return self == null ? null : f(self);
  }
}

extension IterableExtension<T> on Iterable<T> {
  /// Calls [f] on every member of [this] with the element and its index.
  Iterable<R> enumerate<R>(R Function(int, T) f, [int start = 0]) sync* {
    for (final item in this) yield f(start++, item);
  }
}

double? roundDouble(double value, int places) {
  num mod = pow(10.0, places);
  return ((value * mod).round().toDouble() / mod);
}

bool isNotNull<T>(T? value) => value != null;
bool isNull<T>(T? value) => value == null;

extension Ex on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}

extension BuildContextExtension on BuildContext {
  ThemeData get theme => Theme.of(this);
  FormTheme get formTheme => Provider.of<FormTheme>(this);
  AppLocalizations get localization => AppLocalizations.of(this);
}

extension EdgeInsetsDirectionalExtension on EdgeInsetsDirectional {
  EdgeInsetsDirectional copyWith({
    double? start,
    double? top,
    double? end,
    double? bottom,
  }) => EdgeInsetsDirectional.only(
    start: start ?? this.start,
    top: top ?? this.top,
    end: end ?? this.end,
    bottom: bottom ?? this.bottom,
  );
}

/// Calculates the size [text] would have given the parameters.
/// 
/// If one of [style], [textDirection] or [textScaleFactor] is null [context] is required.
Size textSize({
  required String text,
  BuildContext? context,
  TextStyle? style,
  TextDirection? textDirection,
  double? textScaleFactor,
  int? maxLines,
  StrutStyle? strutStyle,
  TextHeightBehavior? textHeightBehavior,
  TextWidthBasis textWidthBasis = TextWidthBasis.parent,
  double minWidth = 0.0,
  double maxWidth = double.infinity,
  String? ellipsis,
  Locale? locale,
  TextAlign textAlign = TextAlign.start,

}) {
  assert(context != null ||
         (style != null && textDirection != null && textScaleFactor != null));
  assert(maxLines == null || maxLines > 0);

  style ??= context!.theme.textTheme.bodyText2;
  textScaleFactor ??= MediaQuery.textScaleFactorOf(context!);

  final textPainter = TextPainter(
    text: TextSpan(text: text, style: style),
    textDirection: textDirection ?? Directionality.of(context!),
    textScaleFactor: textScaleFactor,
    maxLines: maxLines,
    strutStyle: strutStyle,
    textHeightBehavior: textHeightBehavior,
    textWidthBasis: textWidthBasis,
    ellipsis: ellipsis,
    locale: locale,
    textAlign: textAlign,
  );
  textPainter.layout(
    minWidth: minWidth,
    maxWidth: maxWidth,
  );

  return textPainter.size;
}
