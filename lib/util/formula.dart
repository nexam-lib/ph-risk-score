import 'dart:math';

import 'package:phindex/data/data.dart';
import 'package:phindex/models/classes.dart';
import 'package:phindex/models/enums.dart';
import 'package:phindex/util/util.dart';

double? calcZScore(DateTime dayOfClinicalPres, DateTime birth, double tapseCM) {
  var ageDiffDays = dayOfClinicalPres.difference(birth).inDays;
  var ageDiffMonths = ageDiffDays / 30;
  var ageDiffYears = ageDiffDays ~/ 365;

  var mean = 0.911463414634146;
  var sd = -0.117421483886250;
  if (ageDiffMonths <= 3.0) {
    mean = 1.135112808299800;
    sd = -0.142809593524561;
  } else if (ageDiffMonths > 3.0 && ageDiffMonths <= 6.0) {
    mean = 1.310588639785390;
    sd = -0.151560908960098;
  } else if (ageDiffMonths > 6.0 && ageDiffMonths < 12.0) {
    mean = 1.443685021151690;
    sd = -0.156743682267921;
  } else {
    final item = ageMeanPercentileYear.firstWhere((element) => element.age == ageDiffYears);
    mean = item.mean;
    sd = item.sd;
  }

  return roundDouble((mean - tapseCM) / sd, 2);
}

double? calcPaatZScore(double paat, Sex sex, double age) {
  // female = 1, male = 0
  final sexPoints = sex.diverse ? 0 : 1;
  final agePoints = age.toInt();
  return (paat - (77.38 + 0.72 * agePoints + 13.88 * sqrt(agePoints) + 2.02 * sexPoints)) / 12.87+0.17 * agePoints;
}

Growth? calcGrowth(double? bmi, AgePercentile? bmiPercentile, double? height, AgePercentile? heightPercentile) {
  if (bmi == null || bmiPercentile == null || heightPercentile == null || height == null) return null;

  if (bmi < bmiPercentile.p3 || height < heightPercentile.p3) {
    return Growth.failureToThrive;
  } else if (bmi > bmiPercentile.p10 && height > heightPercentile.p10) {
    return Growth.normal;
  } else {
    return Growth.slightlyRestricted;
  }
}

AgePercentile getBmiPercentile(Sex sex, double age) {
  final values = sex.diverse ? bmiBoys : bmiGirls;
  return values.firstWhere((e) => e.age == age);
}

AgePercentile getHeightPercentile(Sex sex, double age) {
  final values = sex.diverse ? heightBoys : heightGirls;
  return values.firstWhere((e) => e.age == age);
}
