import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:phindex/models/classes.dart';
import 'package:phindex/models/enums.dart';

extension AppLocalizationsExtension on AppLocalizations {
  String translateSex(Sex sex) {
    switch (sex) {
      case Sex.female: return sexFemale;
      case Sex.male: return sexMale;
      case Sex.diverse: return sexDiverse;
    }
  }

  String translateRace(Race race) {
    switch (race) {
      case Race.asian: return raceAsian;
      case Race.black: return raceBlack;
      case Race.freetext: return raceFreetext;
      case Race.hispanic: return raceHispanic;
      case Race.white: return raceWhite;
      case Race.other: return raceOther;
    }
  }

  String translateContinent(Continent continent) {
    switch (continent) {
      case Continent.africa: return continentAfrica;
      case Continent.antarctica: return continentAntarctica;
      case Continent.asia: return continentAsia;
      case Continent.europe: return continentEurope;
      case Continent.northAmerica: return continentNorthAmerica;
      case Continent.southAmerica: return continentSouthAmerica;
      case Continent.oceania: return continentOceania;
    }
  }

  String translatePhClassification(PhClassification phClassification) {
    switch (phClassification) {
      case PhClassification.IPAH: return phClassificationIPAH;
      case PhClassification.HPAH: return phClassificationHPAH;
      case PhClassification.PAH_CHD: return phClassificationPAH_CHD;
      case PhClassification.PH_LHD: return phClassificationPH_LHD;
      case PhClassification.PH_CLD: return phClassificationPH_CLD;
      case PhClassification.CTEPH: return phClassificationCTEPH;
      case PhClassification.unclear: return phClassificationUnclear;
      case PhClassification.PH_ComplexCHD: return phClassificationPH_complexCHD;
    }
  }

  String translateGrowth(Growth growth) {
    switch (growth) {
      case Growth.normal: return growthNormal;
      case Growth.slightlyRestricted: return growthSlightlyRestricted;
      case Growth.failureToThrive: return growthFailureToThrive;
    }
  }

  /// Used for both BMI and height percentiles
  String translatePercentile(double? bmi, AgePercentile? percentile) {
    if (bmi == null || percentile == null)
      return "";
    else if (bmi < percentile.p3)
      return "< p3";
    else if (bmi < percentile.p10)
      return "p3 - p10";
    else
      return "> p10";
  }

  String translateWHOFunctionalClass(WHOFunctionalClass whoFunctionalClass) {
    switch (whoFunctionalClass) {
      case WHOFunctionalClass.I: return whoFunctionalClassI;
      case WHOFunctionalClass.II: return whoFunctionalClassII;
      case WHOFunctionalClass.III: return whoFunctionalClassIII;
      case WHOFunctionalClass.IV: return whoFunctionalClassIV;
    }
  }

  String translateSerumNTproBNP(SerumNTproBNP serumNTproBNP) {
    switch (serumNTproBNP) {
      case SerumNTproBNP.minimallyElevated: return serumNTproBNPMinimallyElevated;
      case SerumNTproBNP.moderatedlyElevated: return serumNTproBNPModeratelyElevated;
      case SerumNTproBNP.greatlyElevated: return serumNTproBNPGreatlyElevated;
    }
  }

  String translateRARVEnlarement(RARVEnlargement rarvEnlargement) {
    switch (rarvEnlargement) {
      case RARVEnlargement.minimal: return raRVEnlargementMinimal;
      case RARVEnlargement.moderate: return raRVEnlargementModerate;
      case RARVEnlargement.severe: return raRVEnlargementSevere;
    }
  }

  String translateRVSystolicDysfunction(RVSystolicDysfunction rvSystolicDysfunction) {
    switch (rvSystolicDysfunction) {
      case RVSystolicDysfunction.noDysfunction: return rvSystolicDysfunctionNo;
      case RVSystolicDysfunction.moderateDysfunction: return rvSystolicDysfunctionModerate;
      case RVSystolicDysfunction.dysfunction: return rvSystolicDysfunctionYes;
    }
  }

  String translateRVLVEndsystolicRatio(RVLVEndsystolicRatio rvlvEndsystolicRatio) {
    switch (rvlvEndsystolicRatio) {
      case RVLVEndsystolicRatio.below1: return rvLVEndsystolicRatioLow;
      case RVLVEndsystolicRatio.between1And1Point5: return rvLVEndsystolicRatioMid;
      case RVLVEndsystolicRatio.severe: return rvLVEndsystolicRatioHigh;
    }
  }

  String translateTAPSE(TAPSELevel tapse) {
    switch (tapse) {
      case TAPSELevel.normal: return tapseNormal;
      case TAPSELevel.moderateLower: return tapseModerateLower;
      case TAPSELevel.belowMinus3: return tapseLow;
    }
  }

  String translateSdRatio(SDRatioLevel sdRatio) {
    switch (sdRatio) {
      case SDRatioLevel.below1: return sdRatioLow;
      case SDRatioLevel.between1And1point4: return sdRatioMid;
      case SDRatioLevel.above1Point4: return sdRatioHigh;
    }
  }

  String translatePAAT(PAATLevel paat) {
    switch (paat) {
      case PAATLevel.over100: return paatHigh;
      case PAATLevel.between70And100: return paatMid;
      case PAATLevel.below70: return paatLow;
    }
  }

  String translateCardiacIndex(CardiacIndexLevel cardiacIndex) {
    switch (cardiacIndex) {
      case CardiacIndexLevel.over3: return cardiacIndexHigh;
      case CardiacIndexLevel.between3And2Point5: return cardiacIndexMid;
      case CardiacIndexLevel.below2Point5: return cardiacIndexLow;
    }
  }

  String translateMRAP(MRAPLevel mRAP) {
    switch (mRAP) {
      case MRAPLevel.below10: return mRAPLow;
      case MRAPLevel.between10And15: return mRAPMid;
      case MRAPLevel.over15: return mRAPHigh;
    }
  }

  String translateMPAPmSAP(MPAPmSAPLevel mPAPmSAP) {
    switch (mPAPmSAP) {
      case MPAPmSAPLevel.below0Point5: return mPAPmSAPLow;
      case MPAPmSAPLevel.between0Point5And0Point75: return mPAPmSAPMid;
      case MPAPmSAPLevel.above0Point75: return mPAPmSAPHigh;
    }
  }

  String translateAcuteVasoreactivityTesting(AcuteVasoreactivityTesting avt) {
    switch (avt) {
      case AcuteVasoreactivityTesting.positive: return positive;
      case AcuteVasoreactivityTesting.negative: return negative;
      case AcuteVasoreactivityTesting.notAvailable: return notAvailable;
    }
  }
}
