import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:phindex/util/theme.dart';
import 'package:phindex/util/util.dart';

class CheckboxInfo<T> {
  const CheckboxInfo(this.color, this.label, this.value);

  static List<CheckboxInfo<T>> greenRed<T>(
    String greenLabel,
    T greenValue,
    String redLabel,
    T redValue,
  ) =>
      [
        CheckboxInfo(green, greenLabel, greenValue),
        CheckboxInfo(red, redLabel, redValue),
      ];

  static List<CheckboxInfo<T>> trafficLights<T>({
    required String greenLabel,
    required T greenValue,
    required String yellowLabel,
    required T yellowValue,
    required String redLabel,
    required T redValue,
  }) =>
      [
        CheckboxInfo(green, greenLabel, greenValue),
        CheckboxInfo(yellow, yellowLabel, yellowValue),
        CheckboxInfo(red, redLabel, redValue),
      ];

  final Color color;
  final String label;
  final T value;
}

class Checkboxes<T> extends StatefulWidget {
  const Checkboxes({
    Key? key,
    required this.checkboxInfos,
    this.onChanged,
    this.selectedValue,
    this.labelAlign = TextAlign.end,
    this.editable = true,
  }) : super(key: key);

  Checkboxes.greenRed({
    Key? key,
    required T greenValue,
    required String greenLabel,
    required T redValue,
    required String redLabel,
    void Function(T?)? onChanged,
    T? selectedValue,
    bool editable = true,
    Axis direction = Axis.vertical,
    CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.end,
  }) : this(
          key: key,
          checkboxInfos: [
            CheckboxInfo(green, greenLabel, greenValue),
            CheckboxInfo(red, redLabel, redValue),
          ],
          onChanged: onChanged,
          selectedValue: selectedValue,
          editable: editable,
        );

  final List<CheckboxInfo<T>> checkboxInfos;
  final void Function(T?)? onChanged;
  final TextAlign labelAlign;
  final T? selectedValue;

  /// If set to false, [selectedValue] can be set to null to set all checkboxes
  /// to unselected
  final bool editable;

  State<Checkboxes<T>> createState() => _CheckboxesState<T>();
}

class _CheckboxesState<T> extends State<Checkboxes<T>> {
  /// -1 if none is selected;
  int selectedCheckbox = -1;

  @override
  void didUpdateWidget(Checkboxes<T> old) {
    super.didUpdateWidget(old);

    final selectedValue = widget.selectedValue;
    if (selectedValue != null || !widget.editable)
      selectedCheckbox =
          widget.checkboxInfos.indexWhere((info) => info.value == selectedValue);
  }

  Widget build(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: widget.checkboxInfos
            .enumerate(
              (i, info) => Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2.0),
                      child: SelectableText(
                        info.label,
                        style: TextStyle(color: info.color),
                        textAlign: widget.labelAlign,
                      ),
                    ),
                  ),
                  Checkbox(
                    value: selectedCheckbox == i,
                    fillColor: MaterialStateProperty.all(info.color),
                    onChanged: !widget.editable
                        ? null
                        : (value) {
                            if (value == null) return;

                            if (value) {
                              widget.onChanged?.call(widget.checkboxInfos[i].value);
                              setState(() => selectedCheckbox = i);
                            } else {
                              widget.onChanged?.call(null);
                              setState(() => selectedCheckbox = -1);
                            }
                          },
                  ),
                ],
              ),
            )
            .toList(growable: false),
      );
}
