import 'package:flutter/widgets.dart';

class RequiredBox extends StatelessWidget {
  const RequiredBox({
    Key? key,
    required this.child,
    this.required = false,
    this.crossAxisAlignment = CrossAxisAlignment.baseline,
  }) : super(key: key);

  final Widget child;
  final bool required;
  final CrossAxisAlignment crossAxisAlignment;

  Widget build(BuildContext context) => !required
  ? child
  : Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: crossAxisAlignment,
      textBaseline: crossAxisAlignment == CrossAxisAlignment.baseline
        ? TextBaseline.alphabetic
        : null,
      children: [
        const Text("x  "),
        Expanded(child: child),
      ],
    );
}
