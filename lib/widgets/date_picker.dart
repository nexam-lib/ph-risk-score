import 'package:flutter/material.dart';
import 'package:phindex/util/util.dart';
import 'package:phindex/widgets/required_box.dart';

class DatePicker extends StatefulWidget {
  const DatePicker({
    Key? key,
    required this.label,
    required this.onChanged,
    this.required = false,
    this.hintText,
    this.errorText,
    this.padding,
  }) : super(key: key);

  final String label;
  final String? hintText;
  final String? errorText;
  final void Function(DateTime) onChanged;
  final EdgeInsetsGeometry? padding;
  final bool required;

  @override
  State<DatePicker> createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  final controller = TextEditingController();

  Future<void> pickDate(BuildContext context) async {
    final now = DateTime.now();
    final dateTime = await showDatePicker(
      context: context,
      initialDate: now,
      firstDate: DateTime(1900),
      lastDate: now,
    );

    if (dateTime != null) {
      controller.value = TextEditingValue(text: localizeDate(context, dateTime));
      widget.onChanged(dateTime);
    }
  }

  Widget build(BuildContext context) => Padding(
        padding: widget.padding ?? context.formTheme.fieldPadding,
        child: RequiredBox(
          required: widget.required,
          crossAxisAlignment: CrossAxisAlignment.start,
          child: TextField(
            controller: controller,
            readOnly: true,
            onTap: () => pickDate(context),
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.always,
              label: SelectableText(widget.label),
              errorText: widget.errorText,
              errorMaxLines: 5,
              hintText: widget.hintText ?? context.localization.dateHint,
              suffixIcon: IconButton(
                icon: const Icon(Icons.calendar_today),
                onPressed: () => pickDate(context),
              ),
            ),
          ),
        ),
      );
}
