import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:phindex/util/util.dart';
import 'package:phindex/widgets/required_box.dart';

class FormField extends StatefulWidget {
  const FormField({
    Key? key,
    required this.label,
    this.helperText,
    this.errorText,
    this.hintText,
    this.onChanged,
    this.padding,
    this.inputFormatters,
    this.value,
    this.required = false,
    this.keyboardType = TextInputType.text,
    this.textInputAction = TextInputAction.next,
    this.autocorrect = false,
    this.autofocus = false,
    this.readOnly = false,
    this.minLines = 1,
    this.maxLines = 1,
  }) : super(key: key);

  final String label;
  final String? helperText;
  final String? errorText;
  final String? hintText;
  final void Function(String)? onChanged;
  final EdgeInsetsGeometry? padding;
  final String? value;
  final bool required;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final bool autocorrect;
  final bool autofocus;
  final bool readOnly;
  final int? minLines;
  final int? maxLines;

  _FormFieldState createState() => _FormFieldState();
}

class _FormFieldState extends State<FormField> {
  final controller = TextEditingController();

  void didUpdateWidget(FormField old) {
    super.didUpdateWidget(old);
    final value = widget.value;
    if (value != null) controller.value = TextEditingValue(text: value);

    if (widget.readOnly && !old.readOnly) controller.clear();
  }

  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    final formTheme = context.formTheme;

    return Padding(
      padding: widget.padding ?? formTheme.textFieldPadding,
      child: RequiredBox(
        required: widget.required,
        crossAxisAlignment: CrossAxisAlignment.start,
        child: TextField(
          decoration: InputDecoration(
            hintText: widget.hintText ?? context.localization.formFieldHint,
            label: SelectableText(widget.label),
            helperText: widget.helperText,
            helperMaxLines: 5,
            errorText: widget.errorText,
            errorMaxLines: 5,
          ),
          controller: controller,
          onChanged: widget.onChanged,
          keyboardType: widget.keyboardType,
          textInputAction: widget.textInputAction,
          inputFormatters: widget.inputFormatters,
          autocorrect: widget.autocorrect,
          autofocus: widget.autofocus,
          readOnly: widget.readOnly,
          minLines: widget.minLines,
          maxLines: widget.maxLines,
        ),
      ),
    );
  }
}
