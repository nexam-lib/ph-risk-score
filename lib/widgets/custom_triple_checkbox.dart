import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:phindex/models/fraction.dart';
import 'package:phindex/util/theme.dart';
import 'package:phindex/util/util.dart';
import 'package:phindex/widgets/checkboxes.dart';
import 'package:phindex/widgets/required_box.dart';

class CustomTripleCheckbox<T> extends StatelessWidget {
  CustomTripleCheckbox({
    Key? key,
    required this.values,
    required this.localize,
    required this.selectedValue,
    this.children = const [],
    this.colors = const [green, yellow, red],
    this.onChanged,
    this.label,
    this.required = false,
    this.padding = const EdgeInsetsDirectional.fromSTEB(12.0, 16.0, 5.0, 16.0),
    this.childrenPadding,
    Fraction? proportions,
    this.labelAlign = TextAlign.justify,
  })  : assert(colors.length == 3),
        assert(values.length == 3),
        proportions = proportions ?? Fraction(6, 5),
        super(key: key);

  final String? label;
  final List<T> values;
  final Localizer<T> localize;
  final T? selectedValue;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? childrenPadding;
  final List<Widget> children;
  final List<Color> colors;
  final void Function(T?)? onChanged;
  final bool required;

  /// The proportions of the title-children-Column and the Checkboxes-column
  final Fraction proportions;

  /// Sets [Checkboxes.labelAlign]
  final TextAlign labelAlign;

  Iterable<Widget> getChildrenInterspersedWithPaddings(FormTheme formTheme) sync* {
    if (children.isEmpty) return;

    yield children[0];
    final gap = SizedBox(height: formTheme.customChildrenGap);

    for (final child in children.skip(1)) {
      yield gap;
      yield child;
    }
  }

  Widget build(BuildContext context) {
    final formTheme = context.formTheme;
    return Padding(
      padding: padding ?? formTheme.fieldPadding,
      child: RequiredBox(
        required: required,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: proportions.numerator,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (label != null) ...[
                    SelectableText(label!, style: title),
                    SizedBox(height: formTheme.fieldTitleGap),
                  ],
                  if (children.isNotEmpty && childrenPadding == null)
                    ...getChildrenInterspersedWithPaddings(formTheme)
                  else if (children.isNotEmpty)
                    Padding(
                      padding: childrenPadding!,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: getChildrenInterspersedWithPaddings(formTheme)
                            .toList(growable: false),
                      ),
                    )
                ],
              ),
            ),
            const SizedBox(width: 16.0),
            Flexible(
              flex: proportions.denominator,
              child: Checkboxes(
                selectedValue: selectedValue,
                editable: onChanged != null,
                onChanged: onChanged,
                labelAlign: labelAlign,
                checkboxInfos: [0, 1, 2]
                    .map((i) => CheckboxInfo(colors[i], localize(values[i]), values[i]))
                    .toList(growable: false),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
