import 'package:flutter/material.dart';
import 'package:phindex/util/util.dart';
import 'package:phindex/widgets/required_box.dart';

class Dropdown<T> extends StatefulWidget {
  Dropdown({
    Key? key,
    required this.label,
    required this.values,
    required this.localize,
    required this.onChanged,
    this.hintText,
    this.padding,
    this.required = false,
    this.sortItemsAfterLocalization = false,
  })  : items = makeItems(sortItemsAfterLocalization, values, localize),
        super(key: key);

  static List<DropdownMenuItem<T>> makeItems<T>(bool sort, List<T> values, Localizer<T> localize) {
    final vs = values
      .map((t) => MapEntry(t, localize(t)))
      .toList(growable: false);
    
    if (sort)
      vs.sort((lhs, rhs) => lhs.value.compareTo(rhs.value));
  
    return vs
        .map((e) => DropdownMenuItem(child: SelectableText(e.value), value: e.key))
        .toList(growable: false);
  }

  final String label;
  final String? hintText;
  final List<T> values;
  final Localizer<T> localize;
  final void Function(T?) onChanged;
  final EdgeInsetsGeometry? padding;
  final bool sortItemsAfterLocalization;
  final List<DropdownMenuItem<T>> items;
  final bool required;

  State<Dropdown<T>> createState() => _DropdownState<T>();
}

class _DropdownState<T> extends State<Dropdown<T>> {
  T? value;

  Widget build(BuildContext context) => Padding(
        padding: widget.padding ?? context.formTheme.fieldPadding,
        child: RequiredBox(
          required: widget.required,
          crossAxisAlignment: CrossAxisAlignment.start,
          child: InputDecorator(
            decoration: InputDecoration(
              label: SelectableText(widget.label),
            ),
            child: DropdownButton<T>(
              isExpanded: true,
              isDense: true,
              underline: const SizedBox(),
              items: widget.items,
              value: value,
              hint: Text(widget.hintText ?? context.localization.formFieldHint),
              onChanged: (v) {
                widget.onChanged(v);
                setState(() => value = v);
              },
            ),
          ),
        ),
      );
}
