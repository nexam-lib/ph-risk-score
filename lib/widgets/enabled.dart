import 'package:flutter/widgets.dart';
import 'package:phindex/util/theme.dart';

class Enabled extends StatelessWidget {
  const Enabled({
    Key? key,
    required this.enabled,
    required this.child,
  }) : super(key: key);

  final bool enabled;
  final Widget child;

  @override
  Widget build(BuildContext context) => enabled
    ? child
    : ColorFiltered(
      colorFilter: const ColorFilter.mode(grey, BlendMode.srcIn),
      child: IgnorePointer(
        ignoring: true,
        child: child,
      ),
    );
}
