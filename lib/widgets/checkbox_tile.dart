import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:phindex/models/fraction.dart';
import 'package:phindex/util/theme.dart';
import 'package:phindex/util/util.dart';
import 'package:phindex/widgets/checkboxes.dart';
import 'package:phindex/widgets/required_box.dart';

class CheckboxTile<T> extends StatelessWidget {
  CheckboxTile({
    Key? key,
    required this.label,
    required this.checkboxes,
    this.description,
    this.required = false,
    /// Why 5.0 and not 4.0 or 6.0?
    this.padding = const EdgeInsetsDirectional.fromSTEB(12.0, 16.0, 5.0, 16.0),
    this.editable = true,
    Fraction? proportions,
  }) : 
      proportions = proportions ?? Fraction(2, 1),
      super(key: key);

  CheckboxTile.fromInfos({
    Key? key,
    required String label,
    void Function(T?)? onChanged,
    required List<CheckboxInfo<T>> checkboxInfos,
    Widget? description,
    bool required = false,
    bool editable = true,
    Fraction? proportions,
  }) : this(
          key: key,
          label: label,
          description: description,
          editable: editable,
          proportions: proportions,
          required: required,
          checkboxes: Checkboxes(
            checkboxInfos: checkboxInfos,
            onChanged: onChanged,
          ),
        );

  static CheckboxTile<bool> greenRed({
    Key? key,
    required String label,
    void Function(bool?)? onChanged,
    Widget? description,
    bool required = false,
    bool editable = true,
    Fraction? proportions,
  }) =>
      CheckboxTile(
        key: key,
        label: label,
        description: description,
        editable: editable,
        proportions: proportions,
        required: required,
        checkboxes: Checkboxes.greenRed(
          greenLabel: "No",
          greenValue: false,
          redLabel: "Yes",
          redValue: true,
          onChanged: onChanged,
        ),
      );

  final String label;
  final Widget? description;
  final Checkboxes<T> checkboxes;
  final EdgeInsetsGeometry? padding;
  final bool editable;
  final Fraction proportions;
  final bool required;

  Widget build(BuildContext context) {
    final formTheme = context.formTheme;
    return Padding(
      padding: padding ?? formTheme.fieldPadding,
      child: RequiredBox(
        required: required,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: proportions.numerator,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SelectableText(label, style: title),
                  if (description != null) ...[
                    SizedBox(height: formTheme.fieldTitleGap),
                    description!,
                  ],
                ],
              ),
            ),
            Flexible(
              flex: proportions.denominator,
              child: checkboxes,
            ),
          ],
        ),
      ),
    );
  }
}
