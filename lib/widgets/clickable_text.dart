import 'package:flutter/widgets.dart';

class ClickableText extends StatefulWidget {
  const ClickableText({
    Key? key,
    required this.text,
    required this.onPressed,
    this.style = const TextStyle(decoration: TextDecoration.underline),
  }) : super(key: key);

  final String text;
  final TextStyle? style;
  final void Function() onPressed;

  State<ClickableText> createState() => _ClickableTextState();
}

class _ClickableTextState extends State<ClickableText> {
  bool pressed = false;

  Widget build(BuildContext context) => MouseRegion(
        cursor: SystemMouseCursors.click,
        child: GestureDetector(
            onTapDown: (_) => setState(() => pressed = true),
            onTapUp: (_) {
              widget.onPressed();
              setState(() => pressed = false);
            },
            onTapCancel: () => setState(() => pressed = false),
            child: Text(
              widget.text,
              style: widget.style,
            )),
      );
}
