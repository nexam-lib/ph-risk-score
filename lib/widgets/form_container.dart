import 'package:flutter/widgets.dart';
import 'package:phindex/util/util.dart';

class FormContainer extends StatelessWidget {
  const FormContainer({
    Key? key,
    required this.child,
    this.padding,
    this.margin,
  }) : super(key: key);

  final Widget child;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;

  Widget build(BuildContext context) {
    final theme = context.theme;
    final formTheme = context.formTheme;
    return Padding(
      padding: padding ?? formTheme.containerPadding,
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(12.0)),
          border: Border.all(
            color: theme.dividerColor,
            width: theme.dividerTheme.thickness ?? 1.0,
            style: BorderStyle.solid,
          ),
        ),
        child: Padding(
          padding: margin ?? formTheme.containerMargin,
          child: child,
        ),
      ),
    );
  }
}
